### Installation

```sh
$ cd interpreter
$ mkdir build && cd build/ && cmake ../
$ make
```

### Language features
See [Language.txt](https://bitbucket.org/wymdlon/interpreter/src/master/language.txt)

 - **Task1 - ** is a lexer
 - **Task2 - ** is a parser
 - **Task3 - ** is a interpreter
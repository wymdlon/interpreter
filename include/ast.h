#pragma once
#include "../include/tree.h"
#include <stdbool.h>

typedef enum {
    AstNodeType_UNKNOWN,
    //
    AstNodeType_ASSIGN,
    AstNodeType_PLUS,
    AstNodeType_MINUS,
    AstNodeType_MULT,
    AstNodeType_DIV,
    AstNodeType_MOD,

    AstNodeType_NEG,
        
    AstNodeType_AND,
    AstNodeType_OR,

    AstNodeType_EQUAL,
    AstNodeType_NEQUAL,
    AstNodeType_MORE,
    AstNodeType_LESS,
    AstNodeType_MOREEQUAL, 
    AstNodeType_LESSEQUAL,

    //
    AstNodeType_NUMB,
    AstNodeType_STRING,
    AstNodeType_ID,
    //
    AstNodeType_ARGLIST,

    AstNodeType_BLOCK,
    AstNodeType_IF,
    AstNodeType_WHILE,
    AstNodeType_DECLAREVAR,

    AstNodeType_PROGRAM,
} AstNodeType;

typedef struct __AstNode AstNode;
struct __AstNode {
   AstNodeType type;
   const char * name;
   // @todo extra data
};

AstNode * AstNode_new(AstNodeType type, const char * name);
void AstNode_free(AstNode * self);
void AstTree_free(Tree * astTree);
void AstTree_pretty(Tree * astTree, const char * fileOut);
bool AstTree_equals(Tree * astTree, Tree * testTree);

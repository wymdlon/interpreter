#pragma once 

#include "tree.h"

typedef struct __Environment Environment;
struct __Environment {
    FILE * outputStream;
};

int Interpreter_execute(Tree * astTree, Environment * env);

typedef enum {
   ValueType_NUMBER,
   ValueType_BOOL,
   ValueType_STRING,
   ValueType_ARRAY,
   ValueType_UNDEFINED
} ValueType;

typedef struct __Var Var;
typedef struct __Value Value;
struct __Value {
   ValueType type;
   void * value;
};

void Value_print(Environment * env, Value * self);

Value * Value_newArray();
Value * Value_newNumber(double number);
Value * Value_newBool(bool boolean);
Value * Value_newString(const char * str);
Value * Value_newUndefined(void);
Value * Value_newCopy(Value * origin);

double Value_number(Value * self);
bool Value_bool(Value * self);
char * Value_string(Value * self);

typedef struct __Program Program;
Environment * Program_getStream(Program * program);
void Program_setError(Program * self, char * error);

typedef Value * (*Function)(Program * program, List * values);
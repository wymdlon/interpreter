#pragma once

#include "list.h"
#include <stdbool.h>

typedef enum{
    TokenType_ID,
    
    TokenType_LRPAR, 
    TokenType_RRPAR,
    
    TokenType_LFPAR, 
    TokenType_RFPAR,

    TokenType_LSPAR, 
    TokenType_RSPAR,
    
    TokenType_PLUS,
    TokenType_MINUS,
    TokenType_MULT,
    TokenType_DIV,
    TokenType_POW,
    TokenType_MOD,

    TokenType_SEMCOL,
    TokenType_COMMA,

    TokenType_NEG,
    
    TokenType_ASSGN,
    
    TokenType_AND,
    TokenType_OR,

    TokenType_EQUAL,
    TokenType_NEQUAL,
    TokenType_MORE,
    TokenType_LESS,
    TokenType_MOREEQUAL, 
    TokenType_LESSEQUAL,

    TokenType_IF,
    TokenType_ELSE,
    TokenType_WHILE,    
    TokenType_LET,
    TokenType_TRUE,
    TokenType_FALSE,

    TokenType_STRING,
    
    TokenType_NUMB,

    TokenType_UNK
} TokenType;

typedef struct __Token {
    TokenType type;
    char * lexem;
    int line;
} Token;

Token * Token_new(TokenType type, char * lexem, int line);
void Token_free(Token * self);
bool Token_equals(Token * token1, Token * token2);
char * TokenType_toString(TokenType temp);

int Lexer_splitTokens(const char * input, List * tokens);

void Lexer_clearTokens(List * tokens);

void Lexer_printTokens(List * tokens);


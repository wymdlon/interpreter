#include <stdio.h>
#include <check.h>
#include "../include/list.h"
#include "../include/lexer.h"
#include "../include/tree.h"
#include "../include/ast.h"
#include "../include/parser.h"
#include "../include/string_ext.h"


START_TEST (parser_empty_empty)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);
    Tree * test = Tree_new(AstNode_new(AstNodeType_PROGRAM, strDup("program")));
    ck_assert(AstTree_equals(root, test));
    AstTree_free(root);
    AstTree_free(test);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (parser_vardecl_vardeclTree)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let a = 4;", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);

    Tree * test = Tree_new(AstNode_new(AstNodeType_PROGRAM, strDup("program")));
    Tree * varDecl = Tree_new(AstNode_new(AstNodeType_DECLAREVAR, strDup("declVar")));
    Tree * name = Tree_new(AstNode_new(AstNodeType_ID, strDup("a")));
    Tree * value = Tree_new(AstNode_new(AstNodeType_NUMB, strDup("4")));
    
    List_add(test->children, varDecl);
    List_add(varDecl->children, name);    
    List_add(varDecl->children, value);

    ck_assert(AstTree_equals(root, test));
    AstTree_free(root);
    AstTree_free(test);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (parser_vardeclWithExpr_vardeclTree)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let a = 4 + 9 * 5;", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);

    Tree * test = Tree_new(AstNode_new(AstNodeType_PROGRAM, strDup("program")));
    Tree * varDecl = Tree_new(AstNode_new(AstNodeType_DECLAREVAR, strDup("declVar")));
    Tree * name = Tree_new(AstNode_new(AstNodeType_ID, strDup("a")));
    Tree * plus = Tree_new(AstNode_new(AstNodeType_PLUS , strDup("+")));
    Tree * mult = Tree_new(AstNode_new(AstNodeType_MULT , strDup("*")));
    Tree * value4 = Tree_new(AstNode_new(AstNodeType_NUMB , strDup("4")));
    Tree * value9 = Tree_new(AstNode_new(AstNodeType_NUMB , strDup("9")));
    Tree * value5 = Tree_new(AstNode_new(AstNodeType_NUMB , strDup("5")));
    
    List_add(test->children, varDecl);
    List_add(varDecl->children, name);
    List_add(varDecl->children, plus);
    List_add(plus->children, value4);
    List_add(plus->children, mult);
    List_add(mult->children, value9);
    List_add(mult->children, value5);
    
    ck_assert(AstTree_equals(root, test));
    AstTree_free(root);
    AstTree_free(test);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (parser_if_ifTree)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("if(5 == 5){print(\"Hello\");}", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);

    Tree * test = Tree_new(AstNode_new(AstNodeType_PROGRAM, strDup("program")));
    Tree * ifBlock = Tree_new(AstNode_new(AstNodeType_IF, strDup("if")));
    Tree * eq = Tree_new(AstNode_new(AstNodeType_EQUAL, strDup("==")));
    Tree * valueF = Tree_new(AstNode_new(AstNodeType_NUMB , strDup("5")));
    Tree * valueS = Tree_new(AstNode_new(AstNodeType_NUMB , strDup("5")));
    Tree * block = Tree_new(AstNode_new(AstNodeType_BLOCK , strDup("block")));
    Tree * print = Tree_new(AstNode_new(AstNodeType_ID , strDup("print")));
    Tree * arglist = Tree_new(AstNode_new(AstNodeType_ARGLIST , strDup("arglist")));
    Tree * str = Tree_new(AstNode_new(AstNodeType_STRING , strDup("Hello")));
    
    List_add(test->children, ifBlock);
    List_add(ifBlock->children, eq);
    List_add(ifBlock->children, block);
    List_add(eq->children, valueF);
    List_add(eq->children, valueS);
    List_add(block->children, print);
    List_add(print->children, arglist);
    List_add(arglist->children, str);
    
    ck_assert(AstTree_equals(root, test));
    AstTree_free(root);
    AstTree_free(test);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (parser_ifElse_ifElseTree)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("if(5 == 5){print(\"Hello\");}else {print(\"world\");}", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);

    Tree * test = Tree_new(AstNode_new(AstNodeType_PROGRAM, strDup("program")));
    Tree * ifBlock = Tree_new(AstNode_new(AstNodeType_IF, strDup("if")));
    Tree * eq = Tree_new(AstNode_new(AstNodeType_EQUAL, strDup("==")));
    Tree * valueF = Tree_new(AstNode_new(AstNodeType_NUMB , strDup("5")));
    Tree * valueS = Tree_new(AstNode_new(AstNodeType_NUMB , strDup("5")));
    Tree * block = Tree_new(AstNode_new(AstNodeType_BLOCK , strDup("block")));
    Tree * print = Tree_new(AstNode_new(AstNodeType_ID , strDup("print")));
    Tree * arglist = Tree_new(AstNode_new(AstNodeType_ARGLIST , strDup("arglist")));
    Tree * str = Tree_new(AstNode_new(AstNodeType_STRING , strDup("Hello")));
    Tree * elseBlock = Tree_new(AstNode_new(AstNodeType_BLOCK , strDup("block")));
    Tree * printElse = Tree_new(AstNode_new(AstNodeType_ID , strDup("print")));
    Tree * arglistElse = Tree_new(AstNode_new(AstNodeType_ARGLIST , strDup("arglist")));
    Tree * strElse = Tree_new(AstNode_new(AstNodeType_STRING , strDup("world")));
    
    List_add(test->children, ifBlock);
    List_add(ifBlock->children, eq);
    List_add(ifBlock->children, block);
    List_add(ifBlock->children, elseBlock);
    List_add(eq->children, valueF);
    List_add(eq->children, valueS);
    List_add(block->children, print);
    List_add(print->children, arglist);
    List_add(arglist->children, str);

    List_add(elseBlock->children, printElse);
    List_add(printElse->children, arglistElse);
    List_add(arglistElse->children, strElse);
    
    ck_assert(AstTree_equals(root, test));
    AstTree_free(root);
    AstTree_free(test);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (parser_while_whileTree)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let i = 0; while(i < 5){i = i + 1;}", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);

    Tree * test = Tree_new(AstNode_new(AstNodeType_PROGRAM, strDup("program")));
    Tree * varDecl = Tree_new(AstNode_new(AstNodeType_DECLAREVAR, strDup("declVar")));
    Tree * name = Tree_new(AstNode_new(AstNodeType_ID, strDup("i")));
    Tree * value = Tree_new(AstNode_new(AstNodeType_NUMB, strDup("0")));

    Tree * whileBlock = Tree_new(AstNode_new(AstNodeType_WHILE, strDup("while")));
    Tree * less = Tree_new(AstNode_new(AstNodeType_LESS, strDup("<")));
    Tree * nameWhile = Tree_new(AstNode_new(AstNodeType_ID, strDup("i")));
    Tree * valueWhile = Tree_new(AstNode_new(AstNodeType_NUMB, strDup("5")));

    Tree * block = Tree_new(AstNode_new(AstNodeType_BLOCK , strDup("block")));
    Tree * ass = Tree_new(AstNode_new(AstNodeType_ASSIGN , strDup("=")));
    Tree * nameBlock = Tree_new(AstNode_new(AstNodeType_ID, strDup("i")));
    Tree * plus = Tree_new(AstNode_new(AstNodeType_PLUS , strDup("+")));
    Tree * nameVal1 = Tree_new(AstNode_new(AstNodeType_ID, strDup("i")));
    Tree * nameVal2 = Tree_new(AstNode_new(AstNodeType_NUMB, strDup("1")));

    List_add(test->children, varDecl);
    List_add(test->children, whileBlock);

    List_add(varDecl->children, name);
    List_add(varDecl->children, value);

    List_add(whileBlock->children, less);
    List_add(whileBlock->children, block);

    List_add(less->children, nameWhile);
    List_add(less->children, valueWhile);

    List_add(block->children, ass);

    List_add(ass->children, nameBlock);
    List_add(ass->children, plus);

    List_add(plus->children, nameVal1);
    List_add(plus->children, nameVal2);
    
    ck_assert(AstTree_equals(root, test));
    AstTree_free(root);
    AstTree_free(test);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (parser_array_arrayTree)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let arr = array();push(arr, \"String\");", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);

    Tree * test = Tree_new(AstNode_new(AstNodeType_PROGRAM, strDup("program")));
    Tree * varDecl = Tree_new(AstNode_new(AstNodeType_DECLAREVAR, strDup("declVar")));
    Tree * name = Tree_new(AstNode_new(AstNodeType_ID, strDup("arr")));
    Tree * arr = Tree_new(AstNode_new(AstNodeType_ID , strDup("array")));

    Tree * push = Tree_new(AstNode_new(AstNodeType_ID , strDup("push")));
    Tree * arglist = Tree_new(AstNode_new(AstNodeType_ARGLIST , strDup("arglist")));
    Tree * nameP = Tree_new(AstNode_new(AstNodeType_ID, strDup("arr")));
    Tree * str = Tree_new(AstNode_new(AstNodeType_STRING , strDup("String")));
    
    List_add(test->children, varDecl);
    List_add(test->children, push);

    List_add(varDecl->children, name);
    List_add(varDecl->children, arr);

    List_add(push->children, arglist);
    List_add(arglist->children, nameP);
    List_add(arglist->children, str);
    
    ck_assert(AstTree_equals(root, test));
    AstTree_free(root);
    AstTree_free(test);
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

Suite *test_suite() {
    Suite *s = suite_create("Parser");
    TCase *tc_sample = tcase_create("ParserTest");
    //
    tcase_add_test(tc_sample, parser_empty_empty);
    tcase_add_test(tc_sample, parser_vardecl_vardeclTree);
    tcase_add_test(tc_sample, parser_vardeclWithExpr_vardeclTree);
    tcase_add_test(tc_sample, parser_if_ifTree);
    tcase_add_test(tc_sample, parser_ifElse_ifElseTree);
    tcase_add_test(tc_sample, parser_while_whileTree);
    tcase_add_test(tc_sample, parser_array_arrayTree);
    
    
    //
    suite_add_tcase(s, tc_sample);
    return s;
}

int main(void) {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!
    srunner_run_all(sr, CK_VERBOSE);

    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}

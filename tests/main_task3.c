#include <stdio.h>
#include <check.h>
#include "../include/list.h"
#include "../include/lexer.h"
#include "../include/tree.h"
#include "../include/ast.h"
#include "../include/parser.h"
#include "../include/string_ext.h"
#include "../include/interpreter.h"

START_TEST (interpreter_empty_empty)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);
    char outputBuffer[500] = "";
    FILE * outputBufferStream = fopen("/dev/null", "w"); 
    setbuf(outputBufferStream, outputBuffer);

    Environment env = { .outputStream = outputBufferStream };
    int runStatus = Interpreter_execute(root, &env);
    ck_assert_int_eq(runStatus, 0);
    ck_assert_str_eq("", outputBuffer);
    Lexer_clearTokens(tokens);
    List_free(tokens);
    AstTree_free(root);
    fclose(outputBufferStream);
}
END_TEST

START_TEST (interpreter_varDeclAndPrint_valueVar)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let s = 3;print(3);", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);
    char outputBuffer[500] = "";
    FILE * outputBufferStream = fopen("/dev/null", "w"); 
    setbuf(outputBufferStream, outputBuffer);

    Environment env = { .outputStream = outputBufferStream };
    int runStatus = Interpreter_execute(root, &env);
    ck_assert_int_eq(runStatus, 0);
    Lexer_clearTokens(tokens);
    List_free(tokens);
    AstTree_free(root);
    ck_assert_str_eq(">> 3.000000 \n", outputBuffer);
    fclose(outputBufferStream);
}
END_TEST

START_TEST (interpreter_DivByZero_errorDivByZero)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let s = 0/0;", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);
    char outputBuffer[500] = "";
    FILE * outputBufferStream = fopen("/dev/null", "w"); 
    setbuf(outputBufferStream, outputBuffer);

    Environment env = { .outputStream = outputBufferStream };
    int runStatus = Interpreter_execute(root, &env);
    ck_assert_int_eq(runStatus, 1);
    Lexer_clearTokens(tokens);
    List_free(tokens);
    AstTree_free(root);
    ck_assert_str_eq("Run-time error: Invalid operation. Division by zero", outputBuffer);
    fclose(outputBufferStream);
}
END_TEST

START_TEST (interpreter_ModByZero_errorModByZero)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let s = 94 % 0.95;", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);
    char outputBuffer[500] = "";
    FILE * outputBufferStream = fopen("/dev/null", "w"); 
    setbuf(outputBufferStream, outputBuffer);

    Environment env = { .outputStream = outputBufferStream };
    int runStatus = Interpreter_execute(root, &env);
    ck_assert_int_eq(runStatus, 1);
    Lexer_clearTokens(tokens);
    List_free(tokens);
    AstTree_free(root);
    ck_assert_str_eq("Run-time error: Invalid operation. Mod by zero", outputBuffer);
    fclose(outputBufferStream);
}
END_TEST

START_TEST (interpreter_ifBlock_printFalse)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("if(5 != 5){print(\"True\");} else print(\"False\");", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);
    char outputBuffer[500] = "";
    FILE * outputBufferStream = fopen("/dev/null", "w"); 
    setbuf(outputBufferStream, outputBuffer);

    Environment env = { .outputStream = outputBufferStream };
    int runStatus = Interpreter_execute(root, &env);
    ck_assert_int_eq(runStatus, 0);
    Lexer_clearTokens(tokens);
    List_free(tokens);
    AstTree_free(root);
    ck_assert_str_eq(">> \"False\" \n", outputBuffer);
    fclose(outputBufferStream);
}
END_TEST

START_TEST (interpreter_whileBlock_printZeroOneAndtwo)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let i = 0;while(i < 3){print(i);i = i + 1;}", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);
    char outputBuffer[500] = "";
    FILE * outputBufferStream = fopen("/dev/null", "w"); 
    setbuf(outputBufferStream, outputBuffer);

    Environment env = { .outputStream = outputBufferStream };
    int runStatus = Interpreter_execute(root, &env);
    ck_assert_int_eq(runStatus, 0);
    Lexer_clearTokens(tokens);
    List_free(tokens);
    AstTree_free(root);
    ck_assert_str_eq(">> 0.000000 \n>> 1.000000 \n>> 2.000000 \n", outputBuffer);
    fclose(outputBufferStream);
}
END_TEST


START_TEST (interpreter_substr_printSubstr)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let s = \"Hell\";let sb = substr(s, 2, 5);print(sb);", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);
    char outputBuffer[500] = "";
    FILE * outputBufferStream = fopen("/dev/null", "w"); 
    setbuf(outputBufferStream, outputBuffer);

    Environment env = { .outputStream = outputBufferStream };
    int runStatus = Interpreter_execute(root, &env);
    ck_assert_int_eq(runStatus, 0);
    Lexer_clearTokens(tokens);
    List_free(tokens);
    AstTree_free(root);
    ck_assert_str_eq(">> \"ll\" \n", outputBuffer);
    fclose(outputBufferStream);
}
END_TEST

START_TEST (interpreter_pushAndAt_printAllArray)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let arr = array();\
                                    let size = 0;\
                                    let i = 0;\
                                    push(arr, 1);\
                                    push(arr, 2);\
                                    push(arr, 3);\
                                    push(arr, 4);\
                                    size = count(arr);\
                                    while(i < size){\
                                        print(at(arr, i));\
                                        i = i + 1; \
                                    }", tokens);
    Tree * root = Parser_buildNewAstTree(tokens);
    char outputBuffer[500] = "";
    FILE * outputBufferStream = fopen("/dev/null", "w"); 
    setbuf(outputBufferStream, outputBuffer);

    Environment env = { .outputStream = outputBufferStream };
    int runStatus = Interpreter_execute(root, &env);
    ck_assert_int_eq(runStatus, 0);
    Lexer_clearTokens(tokens);
    List_free(tokens);
    AstTree_free(root);
    ck_assert_str_eq(">> 1.000000 \n\
>> 2.000000 \n\
>> 3.000000 \n\
>> 4.000000 \n", outputBuffer);
    fclose(outputBufferStream);
}
END_TEST

Suite *test_suite() {
    Suite *s = suite_create("Interpreter");
    TCase *tc_sample = tcase_create("InterpreterTest");
    //
    tcase_add_test(tc_sample, interpreter_empty_empty);
    tcase_add_test(tc_sample, interpreter_varDeclAndPrint_valueVar);
    tcase_add_test(tc_sample, interpreter_DivByZero_errorDivByZero);
    tcase_add_test(tc_sample, interpreter_ModByZero_errorModByZero);
    tcase_add_test(tc_sample, interpreter_ifBlock_printFalse);
    tcase_add_test(tc_sample, interpreter_whileBlock_printZeroOneAndtwo);
    tcase_add_test(tc_sample, interpreter_substr_printSubstr);
    tcase_add_test(tc_sample, interpreter_pushAndAt_printAllArray);
    
    //
    suite_add_tcase(s, tc_sample);
    return s;
}

int main(void) {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!
    srunner_run_all(sr, CK_VERBOSE);

    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}

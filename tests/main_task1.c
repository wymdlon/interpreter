#include <stdio.h>
#include <check.h>
#include "../include/list.h"
#include "../include/lexer.h"



START_TEST (split_empty_empty)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 0);
    List_free(tokens);
}
END_TEST

START_TEST (split_oneNumber_oneNumberToken)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("13", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_NUMB, "13" };
    ck_assert(Token_equals(firstToken, &testToken));
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST


START_TEST (split_onePow_onePowToken)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("^", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_POW, "^" };
    ck_assert(Token_equals(firstToken, &testToken));
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (split_invalidChar_errorCode)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("&", tokens);
    ck_assert_int_eq(status, 1);
    ck_assert_int_eq(List_count(tokens), 0);
    List_free(tokens);
    }
END_TEST


START_TEST (split_2Numb_TwoNumberToken)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("2.6 96", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 2);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token * secondToken = (Token *)List_at(tokens, 1);    
    Token testToken1 = { TokenType_NUMB, "2.6" };
    Token testToken2 = { TokenType_NUMB, "96" };
    ck_assert(Token_equals(firstToken, &testToken1));
    ck_assert(Token_equals(secondToken, &testToken2));
    Lexer_clearTokens(tokens);
    List_free(tokens);
    }
END_TEST

START_TEST (split_createVariable_TwoToken)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("let min", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 2);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token * secondToken = (Token *)List_at(tokens, 1);    
    Token testToken1 = { TokenType_LET, "let" };
    Token testToken2 = { TokenType_ID, "min" };
    ck_assert(Token_equals(firstToken, &testToken1));
    ck_assert(Token_equals(secondToken, &testToken2));
    Lexer_clearTokens(tokens);
    List_free(tokens);
    }
END_TEST

START_TEST (split_oneSemicolon_oneSemcolSemicolon)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens(";", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_SEMCOL, ";" };
    ck_assert(Token_equals(firstToken, &testToken));
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (split_allBoolKeyword_twoBoolKeyword)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("true false", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 2);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token * secondToken = (Token *)List_at(tokens, 1);    
    Token testToken1 = { TokenType_TRUE, "true" };
    Token testToken2 = { TokenType_FALSE, "false" };
    ck_assert(Token_equals(firstToken, &testToken1));
    ck_assert(Token_equals(secondToken, &testToken2));
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (split_comparisonOperations_allComparisonOperations)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("> >= == != <= <", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 6);
    Token * token1 = (Token *)List_at(tokens, 0);
    Token * token2 = (Token *)List_at(tokens, 1);
    Token * token3 = (Token *)List_at(tokens, 2);
    Token * token4 = (Token *)List_at(tokens, 3);
    Token * token5 = (Token *)List_at(tokens, 4);
    Token * token6 = (Token *)List_at(tokens, 5);
    
    Token testToken1 = { TokenType_MORE, ">" };
    Token testToken2 = { TokenType_MOREEQUAL, ">=" };
    Token testToken3 = { TokenType_EQUAL, "==" };
    Token testToken4 = { TokenType_NEQUAL, "!=" };
    Token testToken5 = { TokenType_LESSEQUAL, "<=" };
    Token testToken6 = { TokenType_LESS, "<" };

    ck_assert(Token_equals(token1, &testToken1));
    ck_assert(Token_equals(token2, &testToken2));
    ck_assert(Token_equals(token3, &testToken3));
    ck_assert(Token_equals(token4, &testToken4));
    ck_assert(Token_equals(token5, &testToken5));
    ck_assert(Token_equals(token6, &testToken6));
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (split_logicOperation_TwoLogicOperation)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("&& ||", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 2);
    Token * token1 = (Token *)List_at(tokens, 0);
    Token * token2 = (Token *)List_at(tokens, 1);
    
    
    Token testToken1 = { TokenType_AND, "&&" };
    Token testToken2 = { TokenType_OR, "||" };
   

    ck_assert(Token_equals(token1, &testToken1));
    ck_assert(Token_equals(token2, &testToken2));
    
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (split_arithmeticOperations_allarithmeticOperations)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("+ - * / %", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 5);
    Token * token1 = (Token *)List_at(tokens, 0);
    Token * token2 = (Token *)List_at(tokens, 1);
    Token * token3 = (Token *)List_at(tokens, 2);
    Token * token4 = (Token *)List_at(tokens, 3);
    Token * token5 = (Token *)List_at(tokens, 4);
    
    Token testToken1 = { TokenType_PLUS, "+" };
    Token testToken2 = { TokenType_MINUS, "-" };
    Token testToken3 = { TokenType_MULT, "*" };
    Token testToken4 = { TokenType_DIV, "/" };
    Token testToken5 = { TokenType_MOD, "%" };

    ck_assert(Token_equals(token1, &testToken1));
    ck_assert(Token_equals(token2, &testToken2));
    ck_assert(Token_equals(token3, &testToken3));
    ck_assert(Token_equals(token4, &testToken4));
    ck_assert(Token_equals(token5, &testToken5));
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (split_negation_oneNegation)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("!", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_NEG, "!" };
    ck_assert(Token_equals(firstToken, &testToken));
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST


START_TEST (split_comma_oneComma)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens(",", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_COMMA, "," };
    ck_assert(Token_equals(firstToken, &testToken));
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (split_string_oneString)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("\"Hello world\"", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 1);
    Token * firstToken = (Token *)List_at(tokens, 0);
    Token testToken = { TokenType_STRING, "Hello world" };
    ck_assert(Token_equals(firstToken, &testToken));
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST

START_TEST (split_brackets_allBrackets)
{
    List * tokens = List_new();
    int status = Lexer_splitTokens("{ [ ( ) ] }", tokens);
    ck_assert_int_eq(status, 0);
    ck_assert_int_eq(List_count(tokens), 6);
    Token * token1 = (Token *)List_at(tokens, 0);
    Token * token2 = (Token *)List_at(tokens, 1);
    Token * token3 = (Token *)List_at(tokens, 2);
    Token * token4 = (Token *)List_at(tokens, 3);
    Token * token5 = (Token *)List_at(tokens, 4);
    Token * token6 = (Token *)List_at(tokens, 5);
    
    Token testToken1 = { TokenType_LFPAR, "{" };
    Token testToken2 = { TokenType_LSPAR, "[" };
    Token testToken3 = { TokenType_LRPAR, "(" };
    Token testToken4 = { TokenType_RRPAR, ")" };
    Token testToken5 = { TokenType_RSPAR, "]" };
    Token testToken6 = { TokenType_RFPAR, "}" };

    ck_assert(Token_equals(token1, &testToken1));
    ck_assert(Token_equals(token2, &testToken2));
    ck_assert(Token_equals(token3, &testToken3));
    ck_assert(Token_equals(token4, &testToken4));
    ck_assert(Token_equals(token5, &testToken5));
    ck_assert(Token_equals(token6, &testToken6));
    Lexer_clearTokens(tokens);
    List_free(tokens);
}
END_TEST


Suite *test_suite() {
    Suite *s = suite_create("Lexer");
    TCase *tc_sample = tcase_create("SplitTest");
    //
    tcase_add_test(tc_sample, split_empty_empty);
    tcase_add_test(tc_sample, split_oneNumber_oneNumberToken);
    tcase_add_test(tc_sample, split_onePow_onePowToken);
    tcase_add_test(tc_sample, split_invalidChar_errorCode);
    tcase_add_test(tc_sample, split_2Numb_TwoNumberToken);
    tcase_add_test(tc_sample, split_createVariable_TwoToken);
    tcase_add_test(tc_sample, split_oneSemicolon_oneSemcolSemicolon);
    tcase_add_test(tc_sample, split_allBoolKeyword_twoBoolKeyword);
    tcase_add_test(tc_sample, split_comparisonOperations_allComparisonOperations);
    tcase_add_test(tc_sample, split_logicOperation_TwoLogicOperation);
    tcase_add_test(tc_sample, split_arithmeticOperations_allarithmeticOperations);
    tcase_add_test(tc_sample, split_negation_oneNegation);
    tcase_add_test(tc_sample, split_comma_oneComma);
    tcase_add_test(tc_sample, split_string_oneString);
    tcase_add_test(tc_sample, split_brackets_allBrackets);
    
    //
    suite_add_tcase(s, tc_sample);
    return s;
}

int main(void) {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!
    srunner_run_all(sr, CK_VERBOSE);

    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}

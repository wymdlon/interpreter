#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "include/lexer.h"
#include "include/file.h"
#include "include/parser.h"
#include "include/ast.h"
#include "include/interpreter.h"

int main(void) {
    char * fileNameIn = "../main.owl";
    char * fileNameOut = "../main_ast.txt";
    if(fileExists(fileNameIn)){
        int size = getFileSize(fileNameIn);
        char input[size + 1];
        readFileToBuffer(fileNameIn, input, size);
        input[size] = '\0';
        List * tokens = List_new();
        if (0 != Lexer_splitTokens(input, tokens)) {
            Lexer_clearTokens(tokens);
            List_free(tokens);
            return EXIT_FAILURE;   
        }

        Lexer_printTokens(tokens);   
        Lexer_clearTokens(tokens);
        List_free(tokens);
        return EXIT_SUCCESS;
   }
   return EXIT_FAILURE;
}

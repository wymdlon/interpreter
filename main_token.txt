1)	<LET, "let"> <ID, "s"> <ASSGN, "="> <ID, "scan"> <LRPAR, "("> <RRPAR, ")"> <SEMCOL, ";"> 
2)	<LET, "let"> <ID, "i"> <ASSGN, "="> <NUMB, "0"> <SEMCOL, ";"> 
3)	<LET, "let"> <ID, "lenNum"> <ASSGN, "="> <NUMB, "0"> <SEMCOL, ";"> 
4)	<LET, "let"> <ID, "arr"> <ASSGN, "="> <ID, "array"> <LRPAR, "("> <RRPAR, ")"> <SEMCOL, ";"> 
5)	<LET, "let"> <ID, "len"> <ASSGN, "="> <ID, "strlen"> <LRPAR, "("> <ID, "s"> <RRPAR, ")"> <SEMCOL, ";"> 
6)	<LET, "let"> <ID, "size"> <ASSGN, "="> <NUMB, "0"> <SEMCOL, ";"> 
7)	<LET, "let"> <ID, "number"> <ASSGN, "="> <NUMB, "0"> <SEMCOL, ";"> 
8)	<LET, "let"> <ID, "min"> <ASSGN, "="> <NUMB, "0"> <SEMCOL, ";"> 
9)	<LET, "let"> <ID, "max"> <ASSGN, "="> <NUMB, "0"> <SEMCOL, ";"> 
10)	<LET, "let"> <ID, "sum"> <ASSGN, "="> <NUMB, "0"> <SEMCOL, ";"> 
11)	<LET, "let"> <ID, "average"> <ASSGN, "="> <NUMB, "0"> <SEMCOL, ";"> 
13)	<WHILE, "while"> <LRPAR, "("> <ID, "i"> <LESS, "<"> <ID, "len"> <RRPAR, ")"> <LFPAR, "{"> 
14)	<IF, "if"> <LRPAR, "("> <ID, "substr"> <LRPAR, "("> <ID, "s"> <COMMA, ","> <ID, "i"> <COMMA, ","> <NUMB, "1"> <RRPAR, ")"> <EQUAL, "=="> <STRING, " "> <OR, "||"> <ID, "i"> <MINUS, "-"> <ASSGN, "="> <ID, "len"> <MINUS, "-"> <NUMB, "1"> <RRPAR, ")"> <LFPAR, "{"> 
15)	<ID, "number"> <ASSGN, "="> <ID, "strtonum"> <LRPAR, "("> <ID, "substr"> <LRPAR, "("> <ID, "s"> <COMMA, ","> <ID, "i"> <MINUS, "-"> <ID, "lenNum"> <COMMA, ","> <ID, "lenNum"> <PLUS, "+"> <NUMB, "1"> <RRPAR, ")"> <RRPAR, ")"> <SEMCOL, ";"> 
16)	<ID, "push"> <LRPAR, "("> <ID, "arr"> <COMMA, ","> <ID, "number"> <RRPAR, ")"> <SEMCOL, ";"> 
17)	<ID, "lenNum"> <ASSGN, "="> <NUMB, "0"> <SEMCOL, ";"> 
18)	<RFPAR, "}"> 
19)	<ID, "lenNum"> <ASSGN, "="> <ID, "lenNum"> <PLUS, "+"> <NUMB, "1"> <SEMCOL, ";"> 
20)	<ID, "i"> <ASSGN, "="> <ID, "i"> <PLUS, "+"> <NUMB, "1"> <SEMCOL, ";"> 
21)	<RFPAR, "}"> 
22)	<ID, "size"> <ASSGN, "="> <ID, "count"> <LRPAR, "("> <ID, "arr"> <RRPAR, ")"> <SEMCOL, ";"> 
24)	<ID, "print"> <LRPAR, "("> <ID, "size"> <RRPAR, ")"> <SEMCOL, ";"> 
26)	<ID, "i"> <ASSGN, "="> <NUMB, "0"> <SEMCOL, ";"> 
27)	<ID, "min"> <ASSGN, "="> <ID, "at"> <LRPAR, "("> <ID, "arr"> <COMMA, ","> <NUMB, "0"> <RRPAR, ")"> <SEMCOL, ";"> 
28)	<ID, "max"> <ASSGN, "="> <ID, "at"> <LRPAR, "("> <ID, "arr"> <COMMA, ","> <NUMB, "0"> <RRPAR, ")"> <SEMCOL, ";"> 
30)	<WHILE, "while"> <LRPAR, "("> <ID, "i"> <LESS, "<"> <ID, "size"> <RRPAR, ")"> <LFPAR, "{"> 
31)	<ID, "sum"> <ASSGN, "="> <ID, "sum"> <PLUS, "+"> <ID, "at"> <LRPAR, "("> <ID, "arr"> <COMMA, ","> <ID, "i"> <RRPAR, ")"> <SEMCOL, ";"> 
32)	<IF, "if"> <LRPAR, "("> <ID, "at"> <LRPAR, "("> <ID, "arr"> <COMMA, ","> <ID, "i"> <RRPAR, ")"> <MORE, ">"> <ID, "max"> <RRPAR, ")"> <LFPAR, "{"> 
33)	<ID, "max"> <ASSGN, "="> <ID, "at"> <LRPAR, "("> <ID, "arr"> <COMMA, ","> <ID, "i"> <RRPAR, ")"> <SEMCOL, ";"> 
34)	<RFPAR, "}"> 
36)	<IF, "if"> <LRPAR, "("> <ID, "at"> <LRPAR, "("> <ID, "arr"> <COMMA, ","> <ID, "i"> <RRPAR, ")"> <LESS, "<"> <ID, "min"> <RRPAR, ")"> <LFPAR, "{"> 
37)	<ID, "min"> <ASSGN, "="> <ID, "at"> <LRPAR, "("> <ID, "arr"> <COMMA, ","> <ID, "i"> <RRPAR, ")"> <SEMCOL, ";"> 
38)	<RFPAR, "}"> 
39)	<ID, "i"> <ASSGN, "="> <ID, "i"> <PLUS, "+"> <NUMB, "1"> <SEMCOL, ";"> 
40)	<RFPAR, "}"> 
41)	<ID, "average"> <ASSGN, "="> <ID, "sum"> <DIV, "/"> <ID, "size"> <SEMCOL, ";"> 
42)	<ID, "print"> <LRPAR, "("> <ID, "min"> <RRPAR, ")"> <SEMCOL, ";"> 
43)	<ID, "print"> <LRPAR, "("> <ID, "max"> <RRPAR, ")"> <SEMCOL, ";"> 
44)	<ID, "print"> <LRPAR, "("> <ID, "sum"> <RRPAR, ")"> <SEMCOL, ";"> 
45)	<ID, "print"> <LRPAR, "("> <ID, "average"> <RRPAR, ")"> <SEMCOL, ";"> 
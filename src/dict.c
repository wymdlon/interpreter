#include "../include/dict.h"
#include <string.h>


typedef struct {
    char * key;
    void * value;
} KeyValuePair;

KeyValuePair * Kpv_new(char * key, void * value){
    KeyValuePair * self = malloc(sizeof(KeyValuePair));
    self->key = key;
    self->value = value;
    return self;
}

void Kpv_free(KeyValuePair * self){
    free(self->key);
    free(self->value);
    free(self);
}

struct __Dict {
    List * pairs;
};

Dict * Dict_new(void){
    Dict * self = malloc(sizeof(Dict));
    self->pairs = List_new();
    return self;
}
void Dict_free(Dict * self){
    List_free(self->pairs);
    free(self);
}

void Dict_add(Dict * self, char * key, void * value){
    KeyValuePair * pair = Kpv_new(key, value);
    List_add(self->pairs, pair);
}

bool Dict_contains(Dict * self, char * key){
    for(int i = 0, sizeList = List_count(self->pairs); i < sizeList; i++){
        KeyValuePair * pair = List_at(self->pairs, i);
        if(strcmp(pair->key, key) == 0){
            return true;
        }
    }
    return false;
}
void * Dict_get(Dict * self, char * key){
    for(int i = 0, sizeList = List_count(self->pairs); i < sizeList; i++){
        KeyValuePair * pair = List_at(self->pairs, i);
        if(strcmp(pair->key, key) == 0){
            return pair->value;
        }
    }
    assert(0 && "Key not found. Function Dict_get"); 
    return NULL;
}
void * Dict_set(Dict * self, char * key, void * value){
    for(int i = 0, sizeList = List_count(self->pairs); i < sizeList; i++){
        KeyValuePair * pair = List_at(self->pairs, i);
        if(strcmp(pair->key, key) == 0){
            void * oldValue = pair->value;
            pair->value = value;
            return oldValue;
        }
    }
    assert(0 && "Key not found. Function Dict_set");
    return NULL;
}
void * Dict_remove(Dict * self, char * key){
    int index = -1;
    for(int i = 0, sizeList = List_count(self->pairs); i < sizeList; i++){
        KeyValuePair * pair = List_at(self->pairs, i);
        if(strcmp(pair->key, key) == 0){
            index = i;
            break;
        }
    }
    if(index > -1){
        KeyValuePair * removedPair = List_removeAt(self->pairs, index);
        char * oldValue = removedPair->value;
        Kpv_free(removedPair);
        return oldValue;
    }
    assert(0 && "Key not found. Function Dict_remove");
    return NULL;
}
void Dict_clear(Dict * self){
    for(int i = List_count(self->pairs) - 1; i >= 0; i--){
        KeyValuePair * removedPair = List_removeAt(self->pairs, i);
        Kpv_free(removedPair);
    }
}
size_t Dict_count(Dict * self){
    return List_count(self->pairs);
}

void Dict_keys(Dict * self, List * keys){
    for(int i = 0, sizeList = List_count(self->pairs); i < sizeList; i++){
        KeyValuePair * pair = List_at(self->pairs, i);
        List_add(keys, pair->key);
    }
}
void Dict_values(Dict * self, List * values){
    for(int i = 0, sizeList = List_count(self->pairs); i < sizeList; i++){
        KeyValuePair * pair = List_at(self->pairs, i);
        List_add(values, pair->value);
    }
}
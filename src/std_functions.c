#include "../include/std_functions.h"
#include "../include/string_ext.h"
#include "../include/interpreter.h"
#include "../include/string_buffer.h"

#include <string.h>
#include <progbase.h>
#include <math.h>

Value * std_println(Program * program, List * values){
    fprintf(Program_getStream(program)->outputStream,">> ");
    for(int i = 0, sizeList = List_count(values); i < sizeList; i++){
        Value * value = List_at(values, i);
        Value_print(Program_getStream(program), value);
        fprintf(Program_getStream(program)->outputStream," ");
    }
    fprintf(Program_getStream(program)->outputStream,"\n");
    
    return Value_newUndefined();
}
Value * std_scan(Program * program, List * values){
    if(List_count(values) != 0){
        Program_setError(program, strDup("Invalid number of scan arguments"));
        return NULL;
    }
    
    char * str = getString();
    Value * val = Value_newString(str);
    free(str);
    return val;
}

Value * std_strlen(Program * program, List * values){
    if(List_count(values) != 1){
        Program_setError(program, strDup("Invalid number of strlen arguments"));
        return NULL;
    }
    
    Value * strVal = List_at(values, 0);
    if(strVal->type != ValueType_STRING){
        Program_setError(program, strDup("Invalid  strlen argument type"));
        return NULL;
    }

    char * str = Value_string(strVal);
    int len = strlen(str);
    return Value_newNumber(len);
}
Value * std_substr(Program * program, List * values){
    if(List_count(values) != 3){
        Program_setError(program, strDup("Invalid number of substr arguments"));
        return NULL;
    }
    
    Value * strVal = List_at(values, 0);
    if(strVal->type != ValueType_STRING){
        Program_setError(program, strDup("Invalid substr argument type"));
        return NULL;
    }
    Value * begin = List_at(values, 1);
    if(begin->type != ValueType_NUMBER){
        Program_setError(program, strDup("Invalid substr argument type"));
        return NULL;
    }
    if(Value_number(begin) < 0){
        Program_setError(program, strDup("Invalid substr begin value"));
        return NULL;
    }
    Value * countVal = List_at(values, 2);
    if(countVal->type != ValueType_NUMBER){
        Program_setError(program, strDup("Invalid substr argument type"));
        return NULL;
    }
    if(Value_number(countVal) < 0){
        Program_setError(program, strDup("Invalid substr count value"));
        return NULL;
    }
    int start = (int)Value_number(begin);
    char * str = Value_string(strVal);
    StringBuffer * sb = StringBuffer_new();
    for(int i = 0, count = Value_number(countVal); i < count && i + start < strlen(str); i++){
        StringBuffer_appendChar(sb, str[i + start]);
    }
    char * newStr = StringBuffer_toNewString(sb);
    Value * newVal = Value_newString(newStr);
    free(newStr);
    StringBuffer_free(sb);
    return newVal;
}

Value * std_numtostr(Program * program, List * values){
    if(List_count(values) != 1){
        Program_setError(program, strDup("Invalid number of numtostr arguments"));
        return NULL;
    }

    Value * numVal = List_at(values, 0);
    if(numVal->type != ValueType_NUMBER){
        Program_setError(program, strDup("Invalid numtostr argument type"));
        return NULL;
    }
    int size = snprintf(NULL, 0, "%lf", Value_number(numVal));
    char s[size + 1];
    sprintf(s, "%lf", Value_number(numVal));
    char * newStr = strDup(s);
    return Value_newString(newStr);
}
Value * std_strtonum(Program * program, List * values){
    if(List_count(values) != 1){
        Program_setError(program, strDup("Invalid number of strtonum arguments"));
        return NULL;
    }

    Value * strVal = List_at(values, 0);
    if(strVal->type != ValueType_STRING){
        Program_setError(program, strDup("Invalid strtonum argument type"));
        return NULL;
    }
    double newNum = atof(Value_string(strVal));
    return Value_newNumber(newNum);
}

Value * std_array(Program * program, List * values){
    if(List_count(values) != 0){
        Program_setError(program, strDup("Invalid number of array arguments"));
        return NULL;
    }
    return Value_newArray();
}
Value * std_count(Program * program, List * values){
    if(List_count(values) != 1){
        Program_setError(program, strDup("Invalid number of count arguments"));
        return NULL;
    }
    Value * listVal = List_at(values, 0);
    if(listVal->type != ValueType_ARRAY){
        Program_setError(program, strDup("Invalid count argument type"));
        return NULL;
    }
    
    int count = List_count(listVal->value);
    return Value_newNumber(count);
}
Value * std_at(Program * program, List * values){
    if(List_count(values) != 2){
        Program_setError(program, strDup("Invalid number of at arguments"));
        return NULL;
    }
    Value * listVal = List_at(values, 0);
    if(listVal->type != ValueType_ARRAY){
        Program_setError(program, strDup("Invalid count argument type"));
        return NULL;
    }
    Value * number = List_at(values, 1);
    if(number->type != ValueType_NUMBER){
        Program_setError(program, strDup("Invalid count argument type"));
        return NULL;
    }

    Value * val = List_at(listVal->value, Value_number(number));
    return Value_newCopy(val);
    
}
Value * std_push(Program * program, List * values){
    if(List_count(values) != 2){
        Program_setError(program, strDup("Invalid number of push arguments"));
        return NULL;
    }
    Value * listVal = List_at(values, 0);
    if(listVal->type != ValueType_ARRAY){
        Program_setError(program, strDup("Invalid argument type"));
        return NULL;
    }
    Value * newValue = List_at(values, 1);

    switch(newValue->type){
        case ValueType_BOOL: {
            List_add(listVal->value, Value_newBool(Value_bool(newValue)));
            break;
        }
        case ValueType_STRING: {
            List_add(listVal->value, Value_newString(Value_string(newValue)));
            break;
        }
        case ValueType_NUMBER: {
            List_add(listVal->value, Value_newNumber(Value_number(newValue)));
            break;
        }
        default: {
            Program_setError(program, strDup("Invalid argument type"));
            return NULL;
        }
    }
    
    return Value_newUndefined();
}

Value * std_pow(Program * program, List * values){
    if(List_count(values) != 1){
        Program_setError(program, strDup("Invalid number of pow arguments"));
        return NULL;
    }

    Value * numVal = List_at(values, 0);
    if(numVal->type != ValueType_NUMBER){
        Program_setError(program, strDup("Invalid pow argument type"));
        return NULL;
    }
    Value * powVal = List_at(values, 1);
    if(powVal->type != ValueType_NUMBER){
        Program_setError(program, strDup("Invalid pow argument type"));
        return NULL;
    }

    double newNum = powf(Value_number(numVal), Value_number(powVal));
    return Value_newNumber(newNum);
}
Value * std_sqrt(Program * program, List * values){
    if(List_count(values) != 1){
        Program_setError(program, strDup("Invalid number of sqrt arguments"));
        return NULL;
    }

    Value * numVal = List_at(values, 0);
    if(numVal->type != ValueType_NUMBER){
        Program_setError(program, strDup("Invalid sqrt argument type"));
        return NULL;
    }

    double newVal = sqrtf(Value_number(numVal));
    return Value_newNumber(newVal);
}

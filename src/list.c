#include "../include/list.h"
#include "../include/iterator.h"
#include <assert.h>


static const int INITIAL_CAPASITY = 16;

struct __List{
    void ** item;
    int capasity;
    int length;
};

static void incMemory(List * self){
    self->capasity *= 2;
    self->item = realloc(self->item, self->capasity * sizeof(void *));
    
    if(self->item == NULL){
        fprintf(stderr, "Out of memory item\n");
        assert(0);
        abort();
    }
}

static void checkIndex(List * self, int index){
    if(index > self->length){
        assert(0 && "Invalid index");
        fprintf(stderr, "Invalid index\n");
        abort();
    }
}



List * List_new(void){
    List * self = malloc(sizeof(List));
    if(self == NULL){
        fprintf(stderr, "Out of memory self\n");
        assert(0);
        abort();
    }
    self->capasity = INITIAL_CAPASITY;
    self->length = 0;
    self->item = malloc(self->capasity * sizeof(void *));
    
    if(self->item == NULL){
        fprintf(stderr, "Out of memory item\n");
        assert(0);
        abort();
    }

    return self;
}
void List_free(List * self){
    free(self->item);
    free(self);
}

void List_add(List * self, void * value){
    if(self->length == self->capasity){
        incMemory(self);
    }

    self->item[self->length] = value;
    self->length++;
}

void List_insert(List * self, size_t index, void * value){
    checkIndex(self, index);
    if(self->length == self->capasity){
        incMemory(self);
    }
    int count = List_count(self);
    if(count > 0)
        for(int i = count; i > index; i--){
            self->item[i] = self->item[i - 1];
        }
    self->item[index] = value;
    self->length++;
}
void * List_at(List * self, size_t index){
    checkIndex(self, index);
    return self->item[index];
}
void * List_set(List * self, size_t index, void * value){
    checkIndex(self, index);
    void * temp = self->item[index];
    self->item[index] = value;
    return temp;
}
void * List_removeAt(List * self, size_t index){
    checkIndex(self, index);
    void * temp = self->item[index];
    for(int i = index + 1; i < self->length; i++){
        self->item[i - 1] = self->item[i];
    }
    self->length--;
    
    if(self->length <= self->capasity / 3 && self->capasity > INITIAL_CAPASITY){
        self->capasity /= 2;
        self->item = realloc(self->item, self->capasity * sizeof(void *));
    }

    return temp;
}
size_t List_count(List * self){
    return self->length;
}

struct __Iterator{
    List * list;
    int index;
};

static Iterator * Iterator_new(List * list, int index){
    Iterator * self = malloc(sizeof(Iterator));
    self->list = list;
    self->index = index;
    return self;
}

Iterator * List_getNewBeginIterator (List * self){
    return Iterator_new(self, 0);
}

Iterator * List_getNewEndIterator   (List * self){
    return Iterator_new(self, List_count(self));
}


void Iterator_free(Iterator * self){
   free(self); 
}

void * Iterator_value    (Iterator * self){
    assert(self->index < List_count(self->list));

    return List_at(self->list, self->index);
}
void   Iterator_next     (Iterator * self){
    self->index++;
}
void   Iterator_prev     (Iterator * self){
    self->index--;
}
void   Iterator_advance  (Iterator * self, IteratorDistance n){
    self->index += n;
}
bool   Iterator_equals   (Iterator * self, Iterator * other){
    return (self->list == other->list && self->index == other->index);
}
IteratorDistance Iterator_distance (Iterator * begin, Iterator * end){
    return end->index - begin->index;
}

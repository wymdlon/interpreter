#include "../include/string_buffer.h" 
#include "../include/string_ext.h"
#include <string.h>
#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

const int INITIAL_CAPACITY = 16;

typedef struct __StringBuffer {
    char * buffer;
    size_t capacity;
    size_t length;

} StringBuffer;

StringBuffer * StringBuffer_new(void){
    StringBuffer * self = malloc(sizeof(StringBuffer));
    self->capacity = INITIAL_CAPACITY;
    self->buffer = malloc(self->capacity * sizeof(char));
    self->buffer[0] = '\0';
    self->length = 1;
    return self; 
}
void StringBuffer_free(StringBuffer * self){
    free(self->buffer);
    free(self);
}

static void ensureCapacity(StringBuffer * self, int appendLength){
     if(self->length + appendLength > self->capacity){
        while(self->capacity <= appendLength + self->length){
            self->capacity *= 2;
        }
        char * newBuffer = realloc(self->buffer, self->capacity * sizeof(char));
        if(newBuffer == NULL){
            assert(0 && "");
        }
        else {
            self->buffer = newBuffer;
        }
    }
}

void StringBuffer_append(StringBuffer * self, const char * str){
    size_t len = strlen(str);
    
    ensureCapacity(self, len);

    strcat(self->buffer + (self->length - 1), str);
    self->length += len;
}
void StringBuffer_appendChar(StringBuffer * self, char ch){
    
    
    ensureCapacity(self, 1);

    self->buffer[self->length] = '\0';
    self->buffer[self->length - 1] = ch;
    self->length++;
}
void StringBuffer_appendFormat(StringBuffer * self, const char * fmt, ...){
    va_list vlist;
    va_start(vlist, fmt);
    size_t bufsz = vsnprintf(NULL, 0, fmt, vlist);
    char * buf = malloc(bufsz + 1);
    va_start(vlist, fmt);
    vsnprintf(buf, bufsz + 1, fmt, vlist);
    va_end(vlist);
    StringBuffer_append(self, buf);
    free(buf);
}

void StringBuffer_clear(StringBuffer * self){
    self->buffer[0] = '\0';
    self->length = 1;
}
char * StringBuffer_toNewString(StringBuffer * self){
    return strDup(self->buffer);
}
#include "../include/interpreter.h"
#include "../include/ast.h"
#include "../include/string_ext.h"
#include "../include/dict.h"
#include "../include/list.h"
#include "../include/std_functions.h"

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

struct __Program{
    Dict * variables;
    Dict * functions;
    char * error;
    Environment * env;
};

Environment * Program_getStream(Program * program){
    return program->env;
}

typedef struct {
    Function ptr;
} StdFunction;


StdFunction * StdFunction_new(Function ptr){
    StdFunction * self = malloc(sizeof(StdFunction));
    self->ptr = ptr;
    return self;
}
void StdFunction_free(StdFunction * self){
    free(self);
}


Value * eval(Program * program, Tree * node);


void Program_setError(Program * self, char * error){
    self->error = error;
}


Value * Value_new(ValueType type, void * value){
    Value * self = malloc(sizeof(Value));
    self->type = type;
    self->value = value;
    return self;
}
void Value_free(Value * self){
    free(self->value);
    free(self);
}

void ValueType_print(ValueType self){
    switch(self) {
        case ValueType_BOOL: printf("bool"); break; 
        case ValueType_NUMBER: printf("number"); break;
        case ValueType_STRING: printf("string"); break;
        case ValueType_UNDEFINED: printf("undefined"); break;
        default: printf("<?>");
    }
}
void Value_print(Environment * env, Value * self){
    switch(self->type) {
        case ValueType_NUMBER: {
            double val = *(double *)self->value;
            fprintf(env->outputStream, "%lf", val);
            break;
        } 
        case ValueType_BOOL: {
            bool val = *(bool *)self->value;
            fprintf(env->outputStream,"%s", val ? "TRUE" : "FALSE");
            break;
        }
        case ValueType_STRING: {
            char *  val = (char *)self->value;
            fprintf(env->outputStream,"\"%s\"", val);
            break;
        }
        case ValueType_UNDEFINED: {
            fprintf(env->outputStream,"undefined");
            break;
        }
        default: fprintf(env->outputStream,"<?>");
    }
    // ValueType_print(self->type);
}


Value * Value_newArray(){
    List * self = List_new();
    return Value_new(ValueType_ARRAY, self);
}
Value * Value_newNumber(double number){
    double * numberMem = malloc(sizeof(double));
    *numberMem = number;
    return Value_new(ValueType_NUMBER, numberMem);
}
Value * Value_newString(const char * str){
    return Value_new(ValueType_STRING, strDup(str));
}
Value * Value_newBool(bool boolean){
    bool * mem = malloc(sizeof(bool));
    *mem = boolean;
    return Value_new(ValueType_BOOL, mem);
}
Value * Value_newUndefined(void){
    return Value_new(ValueType_UNDEFINED, NULL);
}


double Value_number(Value * self){
    assert(self->type == ValueType_NUMBER);
    double res = *((double *)self->value);
    return *((double *)self->value);
}
bool Value_bool(Value * self){
    assert(self->type == ValueType_BOOL);
    return *((bool *)self->value);
}
char * Value_string(Value * self){
    assert(self->type == ValueType_STRING);
    return ((char *)self->value);
}


bool Value_equals(Value * a, Value * b){
    if(a->type != b->type) return false;
    switch(a->type){
        case ValueType_BOOL:        return Value_bool(a) == Value_bool(b);
        case ValueType_NUMBER:      return fabs(Value_number(a) - Value_number(b)) < 1e-6;
        case ValueType_STRING:      return !strcmp(Value_string(a), Value_string(b));
        case ValueType_UNDEFINED:   return true;
        default: assert(0 && "Not supported");
    }
}
bool Value_more(Value * a, Value * b){
    if(a->type != b->type) return false;
    switch(a->type){
        case ValueType_BOOL: return false;
        case ValueType_NUMBER: return fabs(Value_number(a)) > fabs(Value_number(b));
        case ValueType_STRING: return false;
        case ValueType_UNDEFINED: return false;
        default: assert(0 && "Not supported");
    }
}
bool Value_less(Value * a, Value * b){
    if(a->type != b->type) return false;
    switch(a->type){
        case ValueType_BOOL: return false;
        case ValueType_NUMBER: return fabs(Value_number(a)) < fabs(Value_number(b));
        case ValueType_STRING: return false;
        case ValueType_UNDEFINED: return false;
        default: assert(0 && "Not supported");
    }
}

bool Value_asBool(Value * self){
    switch(self->type){
        case ValueType_BOOL: return Value_bool(self);
        case ValueType_NUMBER: return fabs(Value_number(self)) > 1e-6;
        case ValueType_STRING: return Value_string(self) != NULL; // @todo empty strings are true
        case ValueType_UNDEFINED: return false;
        default: assert(0 && "Not supported");
    }
}

Value * Program_getVariableValue(Program * program, char * varId){
    if(!Dict_contains(program->variables, varId)){
        program->error = strDup("Var is not found");
        return NULL;
    }
    return Dict_get(program->variables, varId);
}

Value * Value_newCopy(Value * origin){
    switch(origin->type){
        case ValueType_BOOL: return Value_newBool(Value_bool(origin));
        case ValueType_NUMBER: return Value_newNumber(Value_number(origin));
        case ValueType_STRING: return Value_newString(Value_string(origin));
        case ValueType_ARRAY: return origin;
        case ValueType_UNDEFINED: assert(0 && "Implement undefined"); return NULL;
        default: assert(0 && "Not supported"); return NULL;
    }
}


Value * eval(Program * program, Tree * node){
    AstNode * astNode = node->value;
    switch(astNode->type){
        case AstNodeType_NUMB: {
            double number = atof(astNode->name);
            return Value_newNumber(number);
        }
        case AstNodeType_STRING: {
            return Value_newString(astNode->name);
        }
        case AstNodeType_ID: {
            char * varId = (char *)astNode->name;
            if(strcmp(varId, "array") == 0) {
                List * arguments = List_new();
                Value * retValue = std_array(program, arguments);
                List_free(arguments);
                return retValue;
            } else if(strcmp(varId, "scan") == 0){
                List * arguments = List_new();
                Value * retValue = std_scan(program, arguments);
                List_free(arguments);
                return retValue;
            } else
            if(List_count(node->children) != 0){
                Tree * argListNode = List_at(node->children, 0);
                AstNode * argList = argListNode->value;
                assert(argList->type == AstNodeType_ARGLIST);
                // 
                if(!Dict_contains(program->functions, varId)){
                    program->error = strDup("Call unknown function");
                    return NULL;
                }
                // 
                List * arguments = List_new();
                for(int i = 0, sizeList = List_count(argListNode->children); i < sizeList; i++){
                    Tree * argListChildNode = List_at(argListNode->children, i);
                    Value * argumentValue = eval(program, argListChildNode);
                    if(program->error) {
                        // @todo free all arguments
                        for(int j = 0, sizeArguments = List_count(arguments); j < sizeArguments; j++){
                            Value * argVal = List_at(arguments, j);
                            if(argVal->type != ValueType_ARRAY) Value_free(argVal);
                        }
                        List_free(arguments);
                        return NULL;
                    }
                    List_add(arguments, argumentValue);
                }
                
                StdFunction * func = Dict_get(program->functions, varId);
                Value * retValue = func->ptr(program, arguments);
                // @todo free all arguments
                for(int j = 0, sizeArguments = List_count(arguments); j < sizeArguments; j++){
                    Value * argVal = List_at(arguments, j);
                    if(argVal->type != ValueType_ARRAY) Value_free(argVal);
                }
                List_free(arguments);
                return retValue;
            } 
            
            Value * varValue = Program_getVariableValue(program, varId);
            if(program->error) return NULL;
            return Value_newCopy(varValue);
        }
        case AstNodeType_ASSIGN: {
            Tree * firstChildNode = List_at(node->children, 0);
            AstNode * firstChild = firstChildNode->value;
            if(firstChild->type != AstNodeType_ID){
                program->error = strDup("Can't assign to rvalue");
                return NULL;
            }
            char * varId = (char *)firstChild->name;
            // 

            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) return NULL;
            // 
            if(!Dict_contains(program->variables, varId)){
                program->error = strDup("Var for assign not found");
                Value_free(secondValue);
                return NULL;
            }
            
            Value * oldValue = Dict_set(program->variables, varId, Value_newCopy(secondValue));
            Value_free(oldValue);
            return secondValue;
        }
        case AstNodeType_PLUS: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER){
                program->error = strDup("Invalid operation");
                Value_free(firstValue);
                return NULL;
            }            
            int nchild = List_count(node->children);
            if(nchild == 1){
                return firstValue;
            } else {
                Tree * secondChild = List_at(node->children, 1);
                Value * secondValue = eval(program, secondChild);
                if(program->error){ 
                    Value_free(firstValue);
                    return NULL;
                }
                if(secondValue->type != ValueType_NUMBER){
                    program->error = strDup("Invalid operation");
                    Value_free(firstValue);
                    Value_free(secondValue);
                    return NULL;
                }
                
                
                double res = Value_number(firstValue) + Value_number(secondValue);
                Value_free(firstValue);
                Value_free(secondValue);
                return Value_newNumber(res);

            }
        }
        case AstNodeType_MINUS: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER){
                program->error = strDup("Invalid operation");
                Value_free(firstValue);
                return NULL;
            }            
            int nchild = List_count(node->children);
            if(nchild == 1){
                *(double *)firstValue->value *= -1;
                return firstValue;
            } else {
                Tree * secondChild = List_at(node->children, 1);
                Value * secondValue = eval(program, secondChild);
                if(program->error){ 
                    Value_free(firstValue);
                    return NULL;
                }
                if(secondValue->type != ValueType_NUMBER){
                    program->error = strDup("Invalid operation");
                    Value_free(firstValue);
                    Value_free(secondValue);
                    return NULL;
                }
                
                
                double res = Value_number(firstValue) - Value_number(secondValue);
                Value_free(firstValue);
                Value_free(secondValue);
                return Value_newNumber(res);

            }
        }
        case AstNodeType_MULT: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER){
                program->error = strDup("Invalid operation");
                Value_free(firstValue);
                return NULL;
            }
            
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error){ 
                Value_free(firstValue);
                return NULL;
            }
            if(secondValue->type != ValueType_NUMBER){
                program->error = strDup("Invalid operation");
                Value_free(firstValue);
                Value_free(secondValue);
                return NULL;
            }
            
            
            double res = Value_number(firstValue) * Value_number(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newNumber(res);
        }
        case AstNodeType_DIV: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER){
                program->error = strDup("Invalid operation");
                Value_free(firstValue);
                return NULL;
            }
            
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error){ 
                Value_free(firstValue);
                return NULL;
            }
            if(secondValue->type != ValueType_NUMBER){
                if(!program->error) program->error = strDup("Invalid operation");
                Value_free(firstValue);
                Value_free(secondValue);
                return NULL;
            }
            if(fabs(Value_number(secondValue)) < 1e-6){
                program->error = strDup("Invalid operation. Division by zero");
                Value_free(firstValue);
                Value_free(secondValue);
                return NULL;
            }
            
            
            double res = Value_number(firstValue) / Value_number(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newNumber(res);
        }
        case AstNodeType_MOD: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER){
                program->error = strDup("Invalid operation");
                Value_free(firstValue);
                return NULL;
            }
            
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error){ 
                Value_free(firstValue);
                return NULL;
            }
            if(secondValue->type != ValueType_NUMBER){
                if(!program->error) program->error = strDup("Invalid operation");
                Value_free(firstValue);
                Value_free(secondValue);
                return NULL;
            }
            if((int)Value_number(secondValue) == 0){
                program->error = strDup("Invalid operation. Mod by zero");
                Value_free(firstValue);
                Value_free(secondValue);
                return NULL;
            }
            
            
            double res = (int)Value_number(firstValue) % (int)Value_number(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newNumber(res);
        }
        case AstNodeType_AND: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) return NULL;
            
            bool res = Value_asBool(firstValue) && Value_asBool(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);
        }
        case AstNodeType_OR: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) return NULL;
            
            bool res = Value_asBool(firstValue) || Value_asBool(secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);
        }
        case AstNodeType_NEG: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            if(firstValue->type != ValueType_NUMBER && firstValue->type != ValueType_BOOL){
                program->error = strDup("Invalid operation");
                Value_free(firstValue);
                return NULL;
            }            
            int nchild = List_count(node->children);
            if(nchild != 1){
                program->error = strDup("Invalid number of not arguments");
                Value_free(firstValue);
                return NULL;
            }
            bool res = Value_asBool(firstValue);
            Value_free(firstValue);
            return Value_newBool(!res);
        }
        case AstNodeType_EQUAL: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) {
                Value_free(firstValue);
                return NULL;
            }
            
            bool res = Value_equals(firstValue, secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);
        }
        case AstNodeType_NEQUAL: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) return NULL;
            
            bool res = !Value_equals(firstValue, secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);
        }
        case AstNodeType_MORE: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) return NULL;
            
            bool res = Value_more(firstValue, secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);
        }
        case AstNodeType_LESS: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) return NULL;
            
            bool res = Value_less(firstValue, secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);
        }
        case AstNodeType_MOREEQUAL: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) return NULL;
            
            bool res = Value_more(firstValue, secondValue) 
                    || Value_equals(firstValue, secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);
        }
        case AstNodeType_LESSEQUAL: {
            Tree * firstChild = List_at(node->children, 0);
            Value * firstValue = eval(program, firstChild);
            if(program->error) return NULL;
            
            Tree * secondChild = List_at(node->children, 1);
            Value * secondValue = eval(program, secondChild);
            if(program->error) return NULL;
            
            bool res = Value_less(firstValue, secondValue) 
                    || Value_equals(firstValue, secondValue);
            Value_free(firstValue);
            Value_free(secondValue);
            return Value_newBool(res);
        }
        default: {
            assert(0 && "Not implicated");
        }
    }

    return NULL;
}


void executeStatement(Program * program, Tree * childNode);

void executeIf(Program * program, Tree * node){
    Tree * exprNode = List_at(node->children, 0);
    Value * testValue = eval(program, exprNode);
    if(program->error) return;
    bool testBool = Value_asBool(testValue);
    Value_free(testValue);
    if(testBool){
        executeStatement(program, List_at(node->children, 1));
    } else if(List_count(node->children) > 2){
        executeStatement(program, List_at(node->children, 2));
    }
}
void executeWhile(Program * program, Tree * node){
    Tree * exprNode = List_at(node->children, 0);
    while(true){
        Value * testValue = eval(program, exprNode);
        if(program->error) return;
        bool testBool = Value_asBool(testValue);
        Value_free(testValue);
        if(!testBool) break;
        // 
        executeStatement(program, List_at(node->children, 1));
        if(program->error) return;
    }
     
}
void executeBlock(Program * program, Tree * blockTreeNode){
    for(int i = 0, sizeTree = List_count(blockTreeNode->children); i < sizeTree; i++){
        if(program->error) break;
        Tree * childNode = List_at(blockTreeNode->children, i);
        // 
        executeStatement(program, childNode);
    }
}
void executeStatement(Program * program, Tree * childNode){
    AstNode * child = childNode->value;
    switch(child->type){
        case AstNodeType_IF: {
            executeIf(program, childNode);
            if(program->error) return;
            break;
        }
        case AstNodeType_WHILE: {
            executeWhile(program, childNode);
            if(program->error) return;
            break;
        }
        case AstNodeType_BLOCK: {
            executeBlock(program, childNode);
            if(program->error) return;
            break;
        }
        default: {
            // expr
            Value * value = eval(program, childNode);  
            if(program->error) return;
            // Value_print(value);
            // puts("");
            Value_free(value);
        }
    }
}

void arrayFree(List * self){
    for(int i = 0, sizeList = List_count(self); i < sizeList; i++){
        Value_free(List_at(self, i));
    }
    List_free(self);
}


int Interpreter_execute(Tree * astTree, Environment * env){
    AstNode * astNode = astTree->value;
    assert(astNode->type == AstNodeType_PROGRAM);
    Program program = {
        .variables = Dict_new(),
        .functions = Dict_new(),
        .error = NULL,
        .env = env
    };

    Dict_add(program.functions, strDup("print"),    StdFunction_new(std_println));
    Dict_add(program.functions, strDup("strlen"),   StdFunction_new(std_strlen));
    Dict_add(program.functions, strDup("scan"),     StdFunction_new(std_scan));
    Dict_add(program.functions, strDup("array"),    StdFunction_new(std_array));
    Dict_add(program.functions, strDup("push"),     StdFunction_new(std_push));
    Dict_add(program.functions, strDup("at"),       StdFunction_new(std_at));
    Dict_add(program.functions, strDup("count"),    StdFunction_new(std_count));
    Dict_add(program.functions, strDup("pow"),      StdFunction_new(std_pow));
    Dict_add(program.functions, strDup("sqrt"),     StdFunction_new(std_sqrt));
    Dict_add(program.functions, strDup("numtostr"), StdFunction_new(std_numtostr));
    Dict_add(program.functions, strDup("strtonum"), StdFunction_new(std_strtonum));
    Dict_add(program.functions, strDup("substr"),   StdFunction_new(std_substr));


    for(int i = 0, sizeTree = List_count(astTree->children); i < sizeTree; i++){
        if(program.error) break;
        Tree * childNode = List_at(astTree->children, i);
        AstNode * child = childNode->value;
        // 
        switch(child->type){
            case AstNodeType_DECLAREVAR: {
                Tree * fChildNode = List_at(childNode->children, 0);
                AstNode * fChild = fChildNode->value;
                Tree * sChildNode = List_at(childNode->children, 1);
                AstNode * sChild = sChildNode->value;
                // 
                char * varName = strDup(fChild->name);
                Value * varValue = eval(&program, sChildNode);
                if(program.error) {
                    free(varName);
                    break;
                }
                if(Dict_contains(program.variables, varName)){
                    program.error = strDup("Dublicate variable id");
                    Value_free(varValue);
                    free(varName);
                    break;
                }
                // 
                Dict_add(program.variables, varName, varValue);
                break;
            }
            case AstNodeType_IF: {
                executeIf(&program, childNode);
                if(program.error) break;
                break;
            }
            case AstNodeType_WHILE: {
                executeWhile(&program, childNode);
                if(program.error) break;
                break;
            }
            case AstNodeType_BLOCK: {
                executeBlock(&program, childNode);
                if(program.error) break;
                break;
            }
            default: {
                // expr
                Value * value = eval(&program, childNode);  
                if(program.error) break;
                // Value_print(value);
                // puts("");
                Value_free(value);
            }
        }
    }
    // @test
    List * keys = List_new();
    Dict_keys(program.variables, keys);

    for(int i = 0, sizeKeys = List_count(keys); i < sizeKeys; i++){
        Value * val = Dict_get(program.variables, List_at(keys, i));
        if(val->type != ValueType_ARRAY) free(val->value);
        else arrayFree(val->value);
    } 
    List_free(keys);
    
    
    Dict_clear(program.functions);
    Dict_clear(program.variables);
    Dict_free(program.functions);
    Dict_free(program.variables);


    if(program.error){
        fprintf(env->outputStream, "Run-time error: %s", program.error);
        free(program.error);
        return 1;
    }
    

    return 0;
}
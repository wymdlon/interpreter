#include <stdlib.h>
#include <string.h>
#include "../include/string_ext.h"

char * strDup(const char *s) {
    char *d = malloc((strlen(s) + 1) * sizeof(char));   // Space for length plus nul
    if (d == NULL) return NULL;          // No memory
    strcpy(d, s);                        // Copy the characters
    return d;                            // Return the new string
}
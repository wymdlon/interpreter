#include "../include/lexer.h"
#include "../include/list.h"
#include "../include/iterator.h"
#include "../include/string_buffer.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

Token * Token_new(TokenType type, char * lexem, int line){
    Token * self = malloc (sizeof(Token));
    self->lexem = lexem;
    self->type = type;
    self->line = line;
    return self;
}

void Token_free(Token * self){
    free(self);
}

bool Token_equals(Token * token1, Token * token2){
    if(!strcmp(token1->lexem, token2->lexem) && token1->type == token2->type)
        return true;
    return false;
}

char * TokenType_toString(TokenType temp){
    StringBuffer * type = StringBuffer_new();

    if(temp == TokenType_ID){
        StringBuffer_append(type, "ID");
    } else if(temp == TokenType_LRPAR){
        StringBuffer_append(type, "LRPAR");
    } else  if(temp == TokenType_RRPAR){
        StringBuffer_append(type, "RRPAR");
    } else if(temp == TokenType_LSPAR){
        StringBuffer_append(type, "LSPAR");
    } else  if(temp == TokenType_RSPAR){
        StringBuffer_append(type, "RSPAR");
    } else if(temp == TokenType_LFPAR){
        StringBuffer_append(type, "LFPAR");
    } else  if(temp == TokenType_RFPAR){
        StringBuffer_append(type, "RFPAR");
    } else if(temp == TokenType_PLUS){
        StringBuffer_append(type, "PLUS");
    } else if(temp == TokenType_MINUS){
        StringBuffer_append(type, "MINUS");
    } else if(temp == TokenType_MULT){
        StringBuffer_append(type, "MULT");
    } else if(temp == TokenType_DIV){
        StringBuffer_append(type, "DIV");
    } else if(temp == TokenType_POW){
        StringBuffer_append(type, "POW");
    } else if(temp == TokenType_MOD){
        StringBuffer_append(type, "MOD"); 
    } else if(temp == TokenType_SEMCOL){
        StringBuffer_append(type, "SEMCOL"); 
    } else if(temp == TokenType_COMMA){
        StringBuffer_append(type, "COMMA"); 
    } else if(temp == TokenType_NEG){
        StringBuffer_append(type, "NEG"); 
    } else if(temp == TokenType_ASSGN){
        StringBuffer_append(type, "ASSGN");
    } else if(temp == TokenType_AND){
        StringBuffer_append(type, "AND");
    } else if(temp == TokenType_OR){
        StringBuffer_append(type, "OR");
    } else if(temp == TokenType_EQUAL){
        StringBuffer_append(type, "EQUAL");
    } else if(temp == TokenType_NEQUAL){
        StringBuffer_append(type, "NEQUAL");
    } else if(temp == TokenType_MORE){
        StringBuffer_append(type, "MORE");
    } else if(temp == TokenType_LESS){
        StringBuffer_append(type, "LESS");
    } else if(temp == TokenType_MOREEQUAL){
        StringBuffer_append(type, "MOREEQUAL");
    } else  if(temp == TokenType_LESSEQUAL){
        StringBuffer_append(type, "LESSEQUAL");
    } else if(temp == TokenType_NUMB){
        StringBuffer_append(type, "NUMB");
    } else if(temp == TokenType_STRING){
        StringBuffer_append(type, "STRING");
    }  else if(temp == TokenType_LET){
        StringBuffer_append(type, "LET");
    } else if(temp == TokenType_IF){
        StringBuffer_append(type, "IF");
    } else if(temp == TokenType_ELSE){
        StringBuffer_append(type, "ELSE");
    } else if(temp == TokenType_WHILE){
        StringBuffer_append(type, "WHILE");
    } else if(temp == TokenType_TRUE){
        StringBuffer_append(type, "TRUE");
    } else if(temp == TokenType_FALSE){
        StringBuffer_append(type, "FALSE");
    }
    char * string = StringBuffer_toNewString(type);
    StringBuffer_free(type);
    return string;
}


int Lexer_splitTokens(const char * input, List * tokens){
    int line = 1;
    for(int i = 0, n = strlen(input); i < n; i++){
        if(input[i] == '\n') line++;
        if(!isspace(input[i])){
            if(input[i] == '+'){
                Token * self = Token_new(TokenType_PLUS, "+", line);
                List_add(tokens, self);
            } else if(input[i] == '-'){
                Token * self = Token_new(TokenType_MINUS, "-", line);
                List_add(tokens, self);
            } else if(input[i] == '/'){
                Token * self = Token_new(TokenType_DIV, "/", line);
                List_add(tokens, self);
            } else if(input[i] == '*'){
                Token * self = Token_new(TokenType_MULT, "*", line);
                List_add(tokens, self);
            } else if(input[i] == '^'){
                Token * self = Token_new(TokenType_POW, "^", line);
                List_add(tokens, self);
            } else if(input[i] == '%'){
                Token * self = Token_new(TokenType_MOD, "%", line);
                List_add(tokens, self);
            }

            else if(input[i] == ';'){
                Token * self = Token_new(TokenType_SEMCOL, ";", line);
                List_add(tokens, self);
            } else if(input[i] == ','){
                Token * self = Token_new(TokenType_COMMA, ",", line);
                List_add(tokens, self);
            }
            
            else if(input[i] == '('){
                Token * self = Token_new(TokenType_LRPAR, "(", line);
                List_add(tokens, self);
            } else if(input[i] == ')'){
                Token * self = Token_new(TokenType_RRPAR, ")", line);
                List_add(tokens, self);
            } 
            
            else if(input[i] == '{'){
                Token * self = Token_new(TokenType_LFPAR, "{", line);
                List_add(tokens, self);
            } else if(input[i] == '}'){
                Token * self = Token_new(TokenType_RFPAR, "}", line);
                List_add(tokens, self);
            } 
            
            else if(input[i] == '['){
                Token * self = Token_new(TokenType_LSPAR, "[", line);
                List_add(tokens, self);
            } else if(input[i] == ']'){
                Token * self = Token_new(TokenType_RSPAR, "]", line);
                List_add(tokens, self);
            }

            else if(input[i] == '>' && input[i + 1] == '='){
                Token * self = Token_new(TokenType_MOREEQUAL, ">=", line);
                List_add(tokens, self);
                i++;
            } else if(input[i] == '<' && input[i + 1] == '='){
                Token * self = Token_new(TokenType_LESSEQUAL, "<=", line);
                List_add(tokens, self);
                i++;
            } else if(input[i] == '=' && input[i + 1] == '='){
                Token * self = Token_new(TokenType_EQUAL, "==", line);
                List_add(tokens, self);
                i++;
            } else if(input[i] == '!' && input[i + 1] == '='){
                Token * self = Token_new(TokenType_NEQUAL, "!=", line);
                List_add(tokens, self);
                i++;
            } else if(input[i] == '>'){
                Token * self = Token_new(TokenType_MORE, ">", line);
                List_add(tokens, self);
            } else if(input[i] == '<'){
                Token * self = Token_new(TokenType_LESS, "<", line);
                List_add(tokens, self);
            } 
            
            else if(input[i] == '&' && input[i + 1] == '&'){
                Token * self = Token_new(TokenType_AND, "&&", line);
                List_add(tokens, self);
                i++;
            } else if(input[i] == '|' && input[i + 1] == '|'){
                Token * self = Token_new(TokenType_OR, "||", line);
                List_add(tokens, self);
                i++;
            } 

            else if(input[i] == '='){
                Token * self = Token_new(TokenType_ASSGN, "=", line);
                List_add(tokens, self);
            }


            else if(input[i] == '!'){
                Token * self = Token_new(TokenType_NEG, "!", line);
                List_add(tokens, self);
            } 

            else if(input[i] == 'i' && input[i + 1] == 'f'){
                Token * self = Token_new(TokenType_IF, "if", line);
                List_add(tokens, self);
                i++;
            } else if(input[i] == 'e' && input[i + 1] == 'l' && input[i + 2] == 's' && input[i + 3] == 'e'){
                Token * self = Token_new(TokenType_ELSE, "else", line);
                List_add(tokens, self);
                i += 3;
            } else if(input[i] == 'w' && input[i + 1] == 'h' && input[i + 2] == 'i' && input[i + 3] == 'l' && input[i + 4] == 'e'){
                Token * self = Token_new(TokenType_WHILE, "while", line);
                List_add(tokens, self);
                i += 4;
            } else if(input[i] == 't' && input[i + 1] == 'r' && input[i + 2] == 'u' && input[i + 3] == 'e'){
                Token * self = Token_new(TokenType_TRUE, "true", line);
                List_add(tokens, self);
                i += 3;
            } else if(input[i] == 'f' && input[i + 1] == 'a' && input[i + 2] == 'l' && input[i + 3] == 's' && input[i + 4] == 'e'){
                Token * self = Token_new(TokenType_FALSE, "false", line);
                List_add(tokens, self);
                i += 4;
            }

            else if(isdigit(input[i])){
                StringBuffer * sb = StringBuffer_new();
                
                while(isdigit(input[i]) || input[i] == '.'){
                    StringBuffer_appendChar(sb, input[i]);
                    i++;
                }
                i--;
                char * bCopy = StringBuffer_toNewString(sb);
                
                StringBuffer_clear(sb);
                StringBuffer_free(sb);

                Token * self = Token_new(TokenType_NUMB, bCopy, line);
                List_add(tokens, self);
            }
            
            else if(input[i] == 'l' && input[i + 1] == 'e' && input[i + 2] == 't'){
                Token * self = Token_new(TokenType_LET, "let", line);
                List_add(tokens, self);
                i += 2;
            }
            else if(input[i] == '_' || isalpha(input[i])){
                    StringBuffer * sb = StringBuffer_new();
                    while(isalnum(input[i])){
                        StringBuffer_appendChar(sb, input[i]);
                        i++;
                    }
                    i--;
                    char * bCopy = StringBuffer_toNewString(sb);
                    
                    StringBuffer_clear(sb);
                    StringBuffer_free(sb);

                    Token * self = Token_new(TokenType_ID, bCopy, line);
                    List_add(tokens, self);
            }
            else if(input[i] == '"'){
                StringBuffer * sb = StringBuffer_new();
                while(input[++i] != '"' && i < n){
                    if(input[i] == '\\' && input[i + 1] == '"'){
                        ++i;
                    }
                    StringBuffer_appendChar(sb, input[i]);
                }

                if(i == n){
                    StringBuffer_free(sb);
                    return 1;
                } 
                char * bCopy = StringBuffer_toNewString(sb);
                    
                StringBuffer_clear(sb);
                StringBuffer_free(sb);

                Token * self = Token_new(TokenType_STRING, bCopy, line);
                List_add(tokens, self);

            } else {
                // printf("Incorrect symbol in %i line", line);
                return 1;
            }
        }
    }
    return 0;
}

void Lexer_clearTokens(List * tokens){
    for(int i = 0, n = List_count(tokens); i < n; i++){
        Token * temp = List_at(tokens, i);
        if(temp->type == TokenType_NUMB || temp->type == TokenType_ID || temp->type == TokenType_STRING){
            free(temp->lexem);
        }
        free(temp);
    }
}

void Lexer_printTokens(List * tokens){
    int flag = 1;
    FILE * f = fopen("../main_token.txt", "w");
    fprintf(f, "%i)\t", flag);

    Iterator * begin = List_getNewBeginIterator(tokens);
    Iterator * end = List_getNewEndIterator(tokens);
    
    while(!Iterator_equals(begin, end)){

        Token * temp = Iterator_value(begin);
        char * type = TokenType_toString(temp->type);
        

        if(flag != temp->line){
            flag = temp->line;
            fprintf(f, "\n%i)\t", flag);
        }

        fprintf(f, "<%s, \"%s\"> ", type, temp->lexem);
        free(type);
        Iterator_next(begin);
    }
    Iterator_free(begin);
    Iterator_free(end);
    fclose(f);

}
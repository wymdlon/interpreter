#include "../include/ast.h"
#include "../include/list.h"
#include "../include/string_ext.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

AstNode * AstNode_new(AstNodeType type, const char * name){
    AstNode * self = malloc(sizeof(AstNode));
    self->type = type;
    self->name = name;
    return self;
}

void AstNode_free(AstNode * self){
    if(self->name) free((void *)self->name);
    free(self);
}

void AstTree_free(Tree * astTree){
    if(astTree == NULL) return;    
    List * children = astTree->children;
    size_t count = List_count(children);
    for(int i = 0; i < count; i++){
        AstTree_free(List_at(children, i));
    }
    AstNode_free(astTree->value);
    Tree_free(astTree);
}

static void PrintPretty(Tree * node, const char * indent, int root, int last, FILE * f);

void AstTree_pretty(Tree * astTree, const char * fileOut){
    if(astTree == NULL) return;
    FILE * f = fopen(fileOut, "w");
    char * indent = strDup("");
    PrintPretty(astTree, indent, 1, 1, f);
    fclose(f);
    free(indent);
}


static char * str_append(const char * str, const char * append){
    size_t newLen = strlen(str) + strlen(append) + 1;
    char * buf = malloc(sizeof(char) * newLen);
    buf[0] = '\0';
    sprintf(buf, "%s%s", str, append);
    return buf;
}

static void PrintPretty(Tree * node, const char * indent, int root, int last, FILE * f){
    
    fprintf(f, "%s", indent);
    char * newIndent = NULL;
    if(last){
        if(!root){
            fprintf(f, "└─");
            newIndent = str_append(indent, "  ");
        } else {
            newIndent = str_append(indent, "");
        }
    } else {
        fprintf(f, "├─");
        newIndent = str_append(indent, "│ ");
    }
    AstNode * astNode = node->value;
    fprintf(f, "%s\n", astNode->name);
    List * children = node->children;
    size_t count = List_count(children);
    
    for(int i = 0; i < count; i++){
        void * child = List_at(children, i);
        PrintPretty(child, newIndent, 0, i == count -1, f);
    }  
    free(newIndent);
}

bool AstTree_equals(Tree * astTree, Tree * testTree){
    if(astTree == NULL && testTree == NULL) return true;
    if(astTree == NULL || testTree == NULL) return false;    
    List * childrensAst = astTree->children;
    List * childrensTest = testTree->children;
    size_t countAst = List_count(childrensAst);
    size_t countTest = List_count(childrensTest);
    if(countAst != countTest) return false;
    bool res = true;
    for(int i = 0; i < countAst && res; i++){
        res = AstTree_equals(List_at(childrensAst, i), List_at(childrensTest, i));
    }
    return res && ((AstNode *)astTree->value)->type == ((AstNode *)testTree->value)->type 
        && 0 == strcmp(((AstNode *)astTree->value)->name, ((AstNode *)testTree->value)->name);
}

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>

#include "../include/list.h"
#include "../include/parser.h"
#include "../include/lexer.h"
#include "../include/iterator.h"
#include "../include/string_buffer.h"
#include "../include/string_ext.h"
#include "../include/ast.h"



typedef struct {
    Iterator * tokens;
    Iterator * tokensEnd;
    char * error;
    int level;
    // optional extra fields
} Parser;

static bool eoi(Parser * self);

#define TRACE_CALL(); /* \
    parser->level++; \
    trace(parser, __func__); \
    Parser * parserPtr __attribute__((cleanup(Parser_decLevel))) = parser;
 */

static void Parser_decLevel(Parser ** parserPtr){
    (*parserPtr)->level--;
}

static void trace(Parser * parser, const char * fn){
    for(int i = 0; i < parser->level; i++){
        printf("..");
    }
    if(eoi(parser)){
        printf("%s: <EOI>\n", fn);
        return;
    }
    Token * token = Iterator_value(parser->tokens);
    char * tokenType = NULL;
    tokenType = TokenType_toString(token->type);
    switch(token->type){
        case TokenType_ID: case TokenType_NUMB: 
        case TokenType_STRING:{
            printf("%s: <%s,%s>\n", fn, tokenType, token->lexem);
            break;
        }
        default:{
            printf("%s: <%s>\n", fn, tokenType);
            break;
        }
    }
    free(tokenType);
}

// check current token type
static Tree * accept(Parser * self, TokenType tokenType);
static bool accept_clear(Parser * self, TokenType tokenType);
// expect exactly that token type or fail
static Tree * expect(Parser * self, TokenType tokenType);
static bool expect_clear(Parser * self, TokenType tokenType);


static Tree * prog        (Parser * parser);
static Tree * var_decl    (Parser * parser);
static Tree * st          (Parser * parser);
static Tree * ID          (Parser * parser);
static Tree * expr        (Parser * parser);
static Tree * expr_st     (Parser * parser);
static Tree * block_st    (Parser * parser);
static Tree * select_st   (Parser * parser);
static Tree * iter_st     (Parser * parser);
static Tree * assign      (Parser * parser);
static Tree * assign_ap   (Parser * parser);
static Tree * log_or      (Parser * parser);
static Tree * log_or_ap   (Parser * parser);
static Tree * log_and     (Parser * parser);
static Tree * log_and_ap  (Parser * parser);
static Tree * eq          (Parser * parser);
static Tree * eq_ap       (Parser * parser);
static Tree * rel         (Parser * parser);
static Tree * rel_ap      (Parser * parser);
static Tree * add         (Parser * parser);
static Tree * add_ap      (Parser * parser);
static Tree * mult        (Parser * parser);
static Tree * mult_ap     (Parser * parser);
static Tree * unary       (Parser * parser);
static Tree * primary     (Parser * parser);
static Tree * NUMBER      (Parser * parser);
static Tree * STRING      (Parser * parser);
static Tree * var_or_call (Parser * parser);
static Tree * parentheses (Parser * parser);
static Tree * fn_call     (Parser * parser);
static Tree * arg_list    (Parser * parser);

Tree * Parser_buildNewAstTree(List * tokens){
    Parser parser = {
        .tokens = List_getNewBeginIterator(tokens),
        .tokensEnd = List_getNewEndIterator(tokens),
        .error = NULL,
        .level = -1
    };
    Tree * progNode = prog(&parser);
    if(parser.error){
        fprintf(stderr, ">>> Error: %s \n", parser.error);
        free(parser.error);
        Iterator_free(parser.tokens);
        Iterator_free(parser.tokensEnd);
        AstTree_free(progNode);
        return NULL;
    } else {
        if(!Iterator_equals(parser.tokens, parser.tokensEnd)){
            Token * token = Iterator_value(parser.tokens);
            char * type = TokenType_toString(token->type);
            fprintf(stderr, ">>> Error: unexpected token %s \n", type);
            free(type);
            Iterator_free(parser.tokens);
            Iterator_free(parser.tokensEnd);
            AstTree_free(progNode);
            return NULL;
        }
    }
    Iterator_free(parser.tokens);
    Iterator_free(parser.tokensEnd);
    return progNode;

}

static bool eoi(Parser * self){
    return Iterator_equals(self->tokens, self->tokensEnd);
}

static AstNodeType TokenType_toAstNodeType(TokenType type){
    switch(type){
        
        case TokenType_ID:          return AstNodeType_ID;
        case TokenType_NUMB:        return AstNodeType_NUMB;
        case TokenType_STRING:      return AstNodeType_STRING;

        case TokenType_ASSGN:       return AstNodeType_ASSIGN;
        case TokenType_PLUS:        return AstNodeType_PLUS;
        case TokenType_MINUS:       return AstNodeType_MINUS;
        case TokenType_MULT:        return AstNodeType_MULT;
        case TokenType_DIV:         return AstNodeType_DIV;
        case TokenType_MOD:         return AstNodeType_MOD;
        case TokenType_EQUAL:       return AstNodeType_EQUAL;
        case TokenType_NEG:         return AstNodeType_NEG;
        case TokenType_NEQUAL:      return AstNodeType_NEQUAL;
        case TokenType_LESS:        return AstNodeType_LESS;
        case TokenType_MORE:        return AstNodeType_MORE;
        case TokenType_LESSEQUAL:   return AstNodeType_LESSEQUAL;
        case TokenType_MOREEQUAL:   return AstNodeType_MOREEQUAL;
        case TokenType_AND:         return AstNodeType_AND;
        case TokenType_OR:          return AstNodeType_OR;
        
        default: return AstNodeType_UNKNOWN;
    }
}

static Tree * accept(Parser * self, TokenType tokenType){
    if(eoi(self)) return NULL;
    Token * token = Iterator_value(self->tokens);
    if(token->type == tokenType){
        AstNodeType astType = TokenType_toAstNodeType(tokenType);
        char * astName = strDup(token->lexem);
        AstNode * node = AstNode_new(astType, astName);
        Tree * tree = Tree_new(node);
        Iterator_next(self->tokens);
        return tree;
    }
    return NULL;
}

static bool accept_clear(Parser * self, TokenType tokenType){
    if(eoi(self)) return false;
    Token * token = Iterator_value(self->tokens);
    if(token->type == tokenType){
        Iterator_next(self->tokens);
        return true;
    }
    return false;
}

static Tree * expect(Parser * self, TokenType tokenType){
    Tree * tree = accept(self, tokenType);
    if(tree != NULL){
        return tree;
    }
    char * currentTokenType = (eoi(self)) 
        ? strDup("end-of-in") 
        : TokenType_toString(((Token *)Iterator_value(self->tokens))->type); 
       
    char * expectTokenType = TokenType_toString(tokenType);
    
    StringBuffer * sb = StringBuffer_new();
    if(!strcmp(currentTokenType, "end-of-in")){
        Iterator_prev(self->tokens);
    }
    StringBuffer_appendFormat(sb, "expected '%s' got '%s', in line %i\n", expectTokenType, currentTokenType, ((Token *)Iterator_value(self->tokens))->line);
    if(self->error == NULL) self->error = StringBuffer_toNewString(sb);
    StringBuffer_free(sb);
    
    free(currentTokenType);
    free(expectTokenType);
    return NULL;
}

static bool expect_clear(Parser * self, TokenType tokenType){
    if(accept_clear(self, tokenType)){
        return true;
    }
    char * currentTokenType = NULL; 
    if(eoi(self)){
        currentTokenType = strDup("end-of-in");
    } else {
        currentTokenType = TokenType_toString(((Token *)Iterator_value(self->tokens))->type);
    }
    
    char * expectTokenType = TokenType_toString(tokenType);
    StringBuffer * sb = StringBuffer_new();
    if(!strcmp(currentTokenType, "end-of-in")){
        Iterator_prev(self->tokens);
    }
    StringBuffer_appendFormat(sb, "expected '%s' got '%s', in line %i\n", expectTokenType, currentTokenType, ((Token *)Iterator_value(self->tokens))->line);
    if(self->error == NULL) self->error = StringBuffer_toNewString(sb);
    StringBuffer_free(sb);
    free(currentTokenType);
    free(expectTokenType);
    return false;
}

typedef Tree * (*GrammarRule)(Parser * parser);

static bool ebnf_multiple(Parser * parser, List * nodes, GrammarRule rule){
    Tree * node = NULL;
    while((node = rule(parser)) && !parser->error){
        List_add(nodes, node);
    }
        ;
        return parser->error == NULL ? true : false;
}

static Tree * ebnf_select(Parser * parser, GrammarRule rules[], size_t rulesLen){
    Tree * node = NULL;
    for(int i = 0; i < rulesLen && !node; i++){
        GrammarRule rule = rules[i];
        node = rule(parser);
        if(parser->error){
            if(node) AstTree_free(node);
            return NULL;
        } 
    }

    return node;
}

static Tree * ebnf_selectLexemes(Parser * parser, TokenType types[], size_t typesLen){
    Tree * node = NULL;
    TokenType type = TokenType_UNK;
    for(int i = 0; i < typesLen && !node; i++){
        node = accept(parser, types[i]);
    }
    return node;
}

static Tree * ebnf_ap_main_rule(Parser * parser, GrammarRule next, GrammarRule ap){
    Tree * nextNode = next(parser);
    if(nextNode){
        Tree * apNode = ap(parser);
        if(apNode){
            List_insert(apNode->children, 0, nextNode);
            return apNode;
        }
        return nextNode;
    }
    return NULL;
}

static Tree * ebnf_ap_recursive_rule(Parser * parser, TokenType types[], size_t typesLen, GrammarRule next, GrammarRule ap){
    if(parser->error) return NULL;
    Tree * opNode = ebnf_selectLexemes(parser, types, typesLen);
    if(opNode == NULL) return NULL;
    
    Tree * node = NULL;
    Tree * nextNode = next(parser);
    Tree * apNode = ap(parser);

    if(apNode){
        List_insert(apNode->children, 0, nextNode);
        node = apNode;
    } else {
        node = nextNode;
    }
    if(nextNode == NULL) {
        parser->error = strDup("Epected expresion");
        AstTree_free(opNode);
        return NULL;
    }
    else List_add(opNode->children, node);
    
    return opNode;
}

static Tree * ID          (Parser * parser){
    TRACE_CALL();
    return accept(parser, TokenType_ID);
}
static Tree * NUMBER      (Parser * parser){
    TRACE_CALL();
    return accept(parser, TokenType_NUMB);
}
static Tree * STRING      (Parser * parser){
    TRACE_CALL();
    return accept(parser, TokenType_STRING);
}
static Tree * BTRUE      (Parser * parser){
    TRACE_CALL();
    return accept(parser, TokenType_TRUE);
}
static Tree * BFALSE      (Parser * parser){
    TRACE_CALL();
    return accept(parser, TokenType_FALSE);
}


static Tree * ID_expect          (Parser * parser){
    TRACE_CALL();
    return expect(parser, TokenType_ID);
}

static Tree * prog        (Parser * parser){
    TRACE_CALL();
    Tree * progNode = Tree_new(AstNode_new(AstNodeType_PROGRAM, strDup("program")));
    (void)ebnf_multiple(parser, progNode->children, var_decl);
    (void)ebnf_multiple(parser, progNode->children, st);

    return progNode;
}
static Tree * var_decl    (Parser * parser){
    TRACE_CALL();
    if(!accept_clear(parser, TokenType_LET)) return NULL;
    Tree * idNode = ID_expect(parser);
    if(!idNode) return NULL;
    if(!expect_clear(parser, TokenType_ASSGN)){
        AstTree_free(idNode);
        return NULL;
    }
    Tree * exprNode = expr(parser);
    if(exprNode == NULL){
        AstTree_free(idNode);
        return NULL;
    }
    if(!expect_clear (parser, TokenType_SEMCOL)){
        AstTree_free(idNode);
        AstTree_free(exprNode);
        return NULL;
    }
    Tree * varDecl = Tree_new(AstNode_new(AstNodeType_DECLAREVAR, strDup("declVar")));
    List_add(varDecl->children, idNode);
    List_add(varDecl->children, exprNode);

    return varDecl;        
}
static Tree * st          (Parser * parser){
    TRACE_CALL();
    return ebnf_select(parser,
    (GrammarRule[]){
        expr_st,
        block_st,
        select_st,
        iter_st     
    }, 4);
    
}

static Tree * st_expect(Parser * parser){
    Tree * stE = st(parser);
    if(stE == NULL){
        if(parser->error) parser->error = strDup("Expect staitmen");
    }

    return stE;
}

static Tree * expr        (Parser * parser){
    TRACE_CALL();
    return assign(parser);
}

static Tree * expr_st     (Parser * parser){
    TRACE_CALL();
    Tree * exprNode = expr(parser);
    if(exprNode){
        expect_clear(parser, TokenType_SEMCOL);
    } else accept_clear(parser, TokenType_SEMCOL);
    return exprNode;
}
static Tree * block_st    (Parser * parser){
    TRACE_CALL();
    if(!accept_clear(parser, TokenType_LFPAR)) return NULL;

    Tree * blockNode = Tree_new(AstNode_new(AstNodeType_BLOCK, strDup("block")));
    (void)ebnf_multiple(parser, blockNode->children, st);
    if(parser->error || expect_clear(parser, TokenType_RFPAR) == false){
        if(blockNode) AstTree_free(blockNode);
        return NULL;
    } 
    return blockNode;
}
static Tree * select_st   (Parser * parser){
    TRACE_CALL();
    if(!accept_clear(parser, TokenType_IF)
        || !expect_clear(parser, TokenType_LRPAR)) return NULL;
    
    Tree * testExprNode = expr(parser);
    if(!testExprNode){
        return NULL;
    }

    if(!expect_clear(parser, TokenType_RRPAR)) {
        AstTree_free(testExprNode);
        return NULL; 
    }

    Tree * stNode = st(parser);
    if(stNode == NULL){
        AstTree_free(testExprNode);
        return NULL;
    }
    Tree * ifNode = Tree_new(AstNode_new(AstNodeType_IF, strDup("if")));
    List_add(ifNode->children, testExprNode);
    List_add(ifNode->children, stNode);
    
    if(accept_clear(parser, TokenType_ELSE)){
        Tree * elseNode = st_expect(parser);
        if(elseNode == NULL || parser->error){
            AstTree_free(ifNode);
            return NULL;
        }
        List_add(ifNode->children, elseNode);
    }

    return ifNode;
}
static Tree * iter_st     (Parser * parser){
    TRACE_CALL();
    if(!accept_clear(parser, TokenType_WHILE)
        || !expect_clear(parser, TokenType_LRPAR)) return NULL;
    
    Tree * testExprNode = expr(parser);
    if(!testExprNode){
        return NULL;
    }

    if(!expect_clear(parser, TokenType_RRPAR)) {
        AstTree_free(testExprNode);
        return NULL; 
    }

    Tree * stNode = st(parser);
    if(stNode == NULL){
        AstTree_free(testExprNode);
        return NULL;
    }
    Tree * whileNode = Tree_new(AstNode_new(AstNodeType_WHILE, strDup("while")));
    List_add(whileNode->children, testExprNode);
    List_add(whileNode->children, stNode);
    
    return whileNode;
}

static Tree * assign      (Parser * parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, log_or, assign_ap);
}
static Tree * assign_ap   (Parser * parser){
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[]){
            TokenType_ASSGN
        }, 1, log_or, assign_ap);
}

static Tree * log_or      (Parser * parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, log_and, log_or_ap);
}
static Tree * log_or_ap   (Parser * parser){
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[]){
            TokenType_OR
        }, 1, log_and, log_or_ap);
}

static Tree * log_and     (Parser * parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, eq, log_and_ap);
}
static Tree * log_and_ap  (Parser * parser){
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[]){
            TokenType_AND
        }, 1, eq, log_and_ap);
}

static Tree * eq          (Parser * parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, rel, eq_ap);
}
static Tree * eq_ap       (Parser * parser){
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[]){
            TokenType_EQUAL,
            TokenType_NEQUAL
        }, 2, rel, eq_ap);
}

static Tree * rel         (Parser * parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, add, rel_ap);
}
static Tree * rel_ap      (Parser * parser){
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[]){
            TokenType_LESS,
            TokenType_LESSEQUAL,
            TokenType_MORE,
            TokenType_MOREEQUAL
        }, 4, add, rel_ap);
}

static Tree * add         (Parser * parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, mult, add_ap);
}
static Tree * add_ap      (Parser * parser){
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[]){
            TokenType_PLUS,
            TokenType_MINUS
        }, 2, mult, add_ap);
}

static Tree * mult        (Parser * parser){
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, unary, mult_ap);
}
static Tree * mult_ap     (Parser * parser){
    return ebnf_ap_recursive_rule(parser, 
        (TokenType[]){
            TokenType_MULT,
            TokenType_DIV,
            TokenType_MOD
        }, 3, unary, mult_ap);
}

static Tree * unary       (Parser * parser){
    TRACE_CALL();
    Tree * opNode = ebnf_selectLexemes(parser, 
        (TokenType[]){
            TokenType_PLUS,
            TokenType_MINUS,
            TokenType_NEG
        },3);

    Tree * primNode = primary(parser);
    
    if(primNode == NULL && opNode != NULL){
        AstTree_free(opNode);
        return NULL;
    }
    if(opNode){
        List_add(opNode->children, primNode);
        return opNode;
    }
    return primNode;
}

static Tree * primary     (Parser * parser){
    TRACE_CALL();
    return ebnf_select(parser,
    (GrammarRule[]){
        NUMBER,
        STRING,
        BTRUE,
        BFALSE,
        var_or_call,
        parentheses
    }, 6);
}

static Tree * var_or_call (Parser * parser){
    TRACE_CALL();
    Tree * varNode = ID(parser);
    if(varNode == NULL) return NULL;
    Tree * argListNode = fn_call(parser);
    if(argListNode) {
        List_add(varNode->children, argListNode);
    }
    return varNode; 
}

static Tree * parentheses (Parser * parser){
    TRACE_CALL();
    if(!accept_clear(parser, TokenType_LRPAR)) return NULL;

    Tree * exprNode = expr(parser);
    if(parser->error || !expect_clear(parser, TokenType_RRPAR)){
        AstTree_free(exprNode);
        return NULL;
    }
    
    return exprNode;
}

static Tree * fn_call     (Parser * parser){
    TRACE_CALL();

    if(!accept_clear(parser, TokenType_LRPAR)) return NULL;

    Tree * argListNode = arg_list(parser);
    if(parser->error || !expect_clear(parser, TokenType_RRPAR)){
        AstTree_free(argListNode);
        return NULL;
    }
    
    return argListNode;
}

static Tree * arg_list    (Parser * parser){
    TRACE_CALL();
    Tree * exprNode = expr(parser);
    if(exprNode){
        Tree * argListNode = Tree_new(AstNode_new(AstNodeType_ARGLIST, strDup("arglist")));
        List_add(argListNode->children, exprNode);
        while(true){
            if(!accept_clear(parser, TokenType_COMMA)) break;
            exprNode = expr(parser);
            if(exprNode){
                List_add(argListNode->children, exprNode);
            }
            else break;
        }
        return argListNode;
    }

    return NULL;
}
